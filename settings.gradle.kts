include(":extensions")
project(":extensions").projectDir = File(rootDir, "features/core/extensions")

include(":core_api")
project(":core_api").projectDir = File(rootDir, "features/core/core_api")

include(":test_utils")
project(":test_utils").projectDir = File(rootDir, "features/core/test_utils")

include(":parsing_impl")
project(":parsing_impl").projectDir = File(rootDir, "features/core/parsing_impl")

include(":network")
project(":network").projectDir = File(rootDir, "features/core/network")

include(":network_debug_tools")
project(":network_debug_tools").projectDir = File(rootDir, "features/core/network_debug_tools")

include(":core_impl")
project(":core_impl").projectDir = File(rootDir, "features/core/core_impl")

include(":core_impl_debug_tools")
project(":core_impl_debug_tools").projectDir = File(rootDir, "features/core/core_impl_debug_tools")

include(":core_ui_api")
project(":core_ui_api").projectDir = File(rootDir, "features/core/core_ui_api")

include(":core_ui_impl")
project(":core_ui_impl").projectDir = File(rootDir, "features/core/core_ui_impl")

include(":core_ui_impl_debug_tools")
project(":core_ui_impl_debug_tools").projectDir = File(rootDir, "features/core/core_ui_impl_debug_tools")

include(":sample_core_api")
project(":sample_core_api").projectDir = File(rootDir, "features/sample/core_api")

include(":sample_impl")
project(":sample_impl").projectDir = File(rootDir, "features/sample/impl")

include(":sample_ui")
project(":sample_ui").projectDir = File(rootDir, "features/sample/ui")

include(":app")