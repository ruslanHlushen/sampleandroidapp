package com.ruslan.hlushan.sample.di

import android.app.Activity
import com.ruslan.hlushan.core.api.di.InjectorHolder
import com.ruslan.hlushan.core.api.di.LoggersProvider
import com.ruslan.hlushan.core.api.di.ManagersProvider
import com.ruslan.hlushan.core.api.di.SchedulersProvider
import com.ruslan.hlushan.sample.SampleAppActivity
import com.ruslan.hlushan.sample.SampleAppPresenter
import com.ruslan.hlushan.ui.api.di.UiCoreProvider
import com.squareup.inject.assisted.dagger2.AssistedModule
import dagger.Component
import dagger.Module

@Component(
    modules = [AssistedSampleMainUiPresentersModule::class],
    dependencies = [
        UiCoreProvider::class,
        ManagersProvider::class,
        LoggersProvider::class,
        SchedulersProvider::class
    ]
)
internal interface SampleMainUiComponent {

    fun inject(activity: SampleAppActivity)

    val sampleAppPresenterFactory: SampleAppPresenter.Factory

    @Component.Factory
    interface Factory {
        fun create(
            uiCoreProvider: UiCoreProvider,
            managersProvider: ManagersProvider,
            loggersProvider: LoggersProvider,
            schedulersProvider: SchedulersProvider
        ): SampleMainUiComponent
    }
}

@AssistedModule
@Module(includes = [AssistedInject_AssistedSampleMainUiPresentersModule::class])
internal abstract class AssistedSampleMainUiPresentersModule

@SuppressWarnings("UnsafeCast")
internal fun Activity.getSampleMainUiComponent(): SampleMainUiComponent {
    val injectorHolder = (this.application as InjectorHolder)
    val components = injectorHolder.components
    return components.getOrPut(SampleMainUiComponent::class) {
        DaggerSampleMainUiComponent.factory()
            .create(
                uiCoreProvider = (injectorHolder.iBaseInjector as UiCoreProvider),
                managersProvider = (injectorHolder.iBaseInjector as ManagersProvider),
                loggersProvider = (injectorHolder.iBaseInjector as LoggersProvider),
                schedulersProvider = (injectorHolder.iBaseInjector as SchedulersProvider)
            )
    }
}