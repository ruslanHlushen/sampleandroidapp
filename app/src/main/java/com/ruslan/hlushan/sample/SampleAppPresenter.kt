package com.ruslan.hlushan.sample

import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.api.managers.SchedulersManager
import com.ruslan.hlushan.sample.ui.screen.list.MuseumsListScreen
import com.ruslan.hlushan.ui.api.presentation.presenter.BasePresenter
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import moxy.InjectViewState
import ru.terrakok.cicerone.Router

@InjectViewState
internal class SampleAppPresenter
@AssistedInject
constructor(
    @Assisted router: Router,
    resourceManager: ResourceManager,
    appLogger: AppLogger,
    schedulersManager: SchedulersManager
) : BasePresenter<SampleAppView>(resourceManager, router, appLogger, schedulersManager) {

    fun startFirstScreen() = router.newRootScreen(MuseumsListScreen())

    @AssistedInject.Factory
    interface Factory {
        fun create(router: Router): SampleAppPresenter
    }
}