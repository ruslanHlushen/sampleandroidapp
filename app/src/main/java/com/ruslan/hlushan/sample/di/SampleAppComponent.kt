package com.ruslan.hlushan.sample.di

import com.ruslan.hlushan.sample.BuildConfig
import com.ruslan.hlushan.core.api.di.CoreProvider
import com.ruslan.hlushan.core.api.di.IBaseInjector
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import com.ruslan.hlushan.core.api.utils.NetworkConfig
import com.ruslan.hlushan.core.impl.di.CoreImplExportComponent
import com.ruslan.hlushan.core.ui.impl.di.UiCoreImplExportComponent
import com.ruslan.hlushan.parsing.impl.di.ParsingExportComponent
import com.ruslan.hlushan.sample.SampleApp
import com.ruslan.hlushan.sample.core.api.di.MuseumsInteractorProvider
import com.ruslan.hlushan.sample.error.ErrorLoggerImpl
import com.ruslan.hlushan.sample.impl.di.SampleImplExportComponentProvider
import com.ruslan.hlushan.ui.api.di.UiCoreProvider
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    dependencies = [
        CoreProvider::class,
        UiCoreProvider::class,
        MuseumsInteractorProvider::class
    ]
)
internal interface SampleAppComponent :
    IBaseInjector,
    CoreProvider,
    UiCoreProvider,
    MuseumsInteractorProvider {

    fun inject(app: SampleApp)

    @Component.Factory
    interface Factory {
        @SuppressWarnings("LongParameterList")
        fun create(
            coreProvider: CoreProvider,
            uiCoreProvider: UiCoreProvider,
            museumsInteractorProvider: MuseumsInteractorProvider
        ): SampleAppComponent
    }

    companion object Initializer {

        fun init(app: SampleApp, initAppConfig: InitAppConfig): SampleAppComponent {

            val networkConfig = NetworkConfig(
                isNetworkLogEnabled = BuildConfig.IS_NETWORK_LOG_ENABLED
            )

            val parserProvider = ParsingExportComponent.Initializer.init()

            val coreProvider = CoreImplExportComponent.Initializer.init(
                baseApplication = app,
                initAppConfig = initAppConfig,
                errorLogger = ErrorLoggerImpl()
            )

            val uiCoreProvider = UiCoreImplExportComponent.Initializer.init(
                external = emptyList(),
                coreProvider = coreProvider
            )

            val sampleImplExportComponentProvider =
                SampleImplExportComponentProvider.Initializer.init(
                    initAppConfig = initAppConfig,
                    networkConfig = networkConfig,
                    appContextProvider = coreProvider,
                    loggersProvider = coreProvider,
                    schedulersProvider = coreProvider,
                    converterFactoryProvider = parserProvider
                )

            return DaggerSampleAppComponent.factory()
                .create(
                    coreProvider = coreProvider,
                    uiCoreProvider = uiCoreProvider,
                    museumsInteractorProvider = sampleImplExportComponentProvider
                )
        }
    }
}