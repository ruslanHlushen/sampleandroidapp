package com.ruslan.hlushan.sample

import com.ruslan.hlushan.core.api.di.ClassInstanceMap
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import com.ruslan.hlushan.core.impl.BaseApplication
import com.ruslan.hlushan.extensions.lazyUnsafe
import com.ruslan.hlushan.sample.di.SampleAppComponent
import com.ruslan.hlushan.sample.error.ErrorLoggerImpl

internal class SampleApp : BaseApplication() {

    override val APP_TAG: String get() = BuildConfig.APP_TAG

    override val initAppConfig: InitAppConfig by lazyUnsafe {
        InitAppConfig(
            appTag = APP_TAG,
            versionCode = BuildConfig.VERSION_CODE,
            versionName = BuildConfig.VERSION_NAME,
            isLogcatEnabled = BuildConfig.IS_LOGCAT_ENABLED,
            isFileLogEnabled = BuildConfig.IS_LOGCAT_ENABLED,
            fileLogsFolder = this.applicationContext.cacheDir
        )
    }

    override val components: ClassInstanceMap = ClassInstanceMap()

    override val iBaseInjector: SampleAppComponent by lazyUnsafe {
        SampleAppComponent.init(this, initAppConfig)
    }

    override fun initDagger2AndInject() = iBaseInjector.inject(this)

    override fun initCrashlytics() = ErrorLoggerImpl.init(this)
}