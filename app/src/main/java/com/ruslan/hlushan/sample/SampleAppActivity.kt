package com.ruslan.hlushan.sample

import androidx.annotation.StyleRes
import com.ruslan.hlushan.sample.di.getSampleMainUiComponent
import com.ruslan.hlushan.ui.api.presentation.view.activity.BaseAppActivity
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.support.SupportAppNavigator

internal class SampleAppActivity : BaseAppActivity<SampleAppPresenter>(), SampleAppView {

    @get:StyleRes
    override val appThemeReId: Int get() = R.style.AppTheme

    @InjectPresenter
    lateinit var sampleAppPresenter: SampleAppPresenter

    @ProvidePresenter
    override fun providePresenter(): SampleAppPresenter =
        getSampleMainUiComponent().sampleAppPresenterFactory.create(appCicerone.router)

    override val presenter: SampleAppPresenter get() = sampleAppPresenter

    override fun initDagger2() = getSampleMainUiComponent().inject(this)

    override fun createNavigator(): Navigator = SupportAppNavigator(this, appContainerResId)

    override fun setUpFirstAppScreen() = presenter.startFirstScreen()
}