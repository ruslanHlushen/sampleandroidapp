
object Plugins {
    const val androidJarPlugin = "com.stepango.androidjar"
    const val detektPlugin = "io.gitlab.arturbosch.detekt"
    const val scabbardPlugin = "scabbard.gradle"

    const val androidGradlePluginClasspath = "com.android.tools.build:gradle:${Versions.gradleVersion}"
    const val kotlinPluginClasspath = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlinVersion}"
    const val proguardPluginClasspath = "net.sf.proguard:proguard-gradle:${Versions.proguardVersion}"
    const val detektFormattingPlugin = "${detektPlugin}:detekt-formatting:${Versions.detektVersion}"
    const val scabbardPluginClasspath = "gradle.plugin.dev.arunkumar:scabbard-gradle-plugin:${Versions.scabbardPluginVersion}"
}