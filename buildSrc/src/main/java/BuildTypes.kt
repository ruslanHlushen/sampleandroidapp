
object BuildTypes {
    const val debug = "debug"
    const val staging = "staging"
    const val release = "release"
}