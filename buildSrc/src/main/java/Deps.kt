
object Deps {

    const val kotlinStdLib = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlinVersion}"

    const val rxJava2 = "io.reactivex.rxjava2:rxjava:${Versions.rxJava2Version}"
    const val rxJava2Android = "io.reactivex.rxjava2:rxandroid:${Versions.rxJava2AndroidVersion}"

    const val dagger2 = "com.google.dagger:dagger:${Versions.dagger2Version}"
    const val dagger2Compiler = "com.google.dagger:dagger-compiler:${Versions.dagger2Version}"

    const val dagger2InjectAssistedAnnotations = "com.squareup.inject:assisted-inject-annotations-dagger2:${Versions.dagger2InjectAssistedVersion}"
    const val dagger2InjectAssistedCompiler = "com.squareup.inject:assisted-inject-processor-dagger2:${Versions.dagger2InjectAssistedVersion}"

    const val gson = "com.google.code.gson:gson:${Versions.gsonVersion}"

    const val supportAnnotations = "androidx.annotation:annotation:${Versions.supportLibraryVersion}"
    const val supportAppCompat = "androidx.appcompat:appcompat:${Versions.supportLibraryVersion}"
    const val supportDesign = "com.google.android.material:material:${Versions.supportLibraryVersion}"
    const val ktx = "androidx.core:core-ktx:${Versions.supportLibraryVersion}"

    const val glide = "com.github.bumptech.glide:glide:${Versions.glideVersion}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glideVersion}"

    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.supportLibraryVersion}"
    const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:${Versions.swipeRefreshLayoutVersion}"
    const val cardView = "androidx.cardview:cardview:${Versions.cardViewVersion}"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayoutVersion}"

    const val moxyCore = "com.github.moxy-community:moxy:${Versions.moxyVersion}"
    const val moxy = "com.github.moxy-community:moxy-androidx:${Versions.moxyVersion}"
    const val moxyCompiler = "com.github.moxy-community:moxy-compiler:${Versions.moxyVersion}"

    const val cicerone = "ru.terrakok.cicerone:cicerone:${Versions.ciceroneVersion}"

    const val okHttp3 = "com.squareup.okhttp3:okhttp:${Versions.okhttp3Version}"
    const val okHttp3Logging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttp3Version}"
    const val okio = "com.squareup.okio:okio:${Versions.okioVersion}"

    const val retrofit2 = "com.squareup.retrofit2:retrofit:${Versions.retrofit2Version}"
    const val retrofit2GsonConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit2Version}"
    const val retrofit2RxJava2Adapter = "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofit2Version}"

    const val binaryPrefs = "com.github.yandextaxitech:binaryprefs:${Versions.binaryPrefsVersion}"

    const val stetho = "com.facebook.stetho:stetho:${Versions.stethoVersion}"

    const val chucker = "com.github.ChuckerTeam.Chucker:library:${Versions.chuckerVersion}"

    const val leakCanary = "com.squareup.leakcanary:leakcanary-android:${Versions.leakCanaryVersion}"

    const val jUnit = "junit:junit:${Versions.jUnitVersion}"
}