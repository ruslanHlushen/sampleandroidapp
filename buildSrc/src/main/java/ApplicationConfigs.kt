
object ApplicationConfigs {
    const val minSdkVersion = 21// don't forget about proguard
    const val targetSdkVersion = 29
    const val compileSdkVersion = targetSdkVersion
    const val buildToolsVersion = "29.0.3"

    const val defaultProguardFile = "proguard-android-optimize.txt"
    const val defaultProguardOkHttp3File = "proguard-rules-okhttp3.pro"
}