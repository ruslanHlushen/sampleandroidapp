
object Versions {

    const val gradleVersion = "3.6.3"

    const val kotlinVersion = "1.3.70"

    const val proguardVersion = "6.2.2"//TODO: check 7.0.0!!!!!!

    const val detektVersion = "1.10.0"
    const val androidJarPluginVersion = "0.1"
    const val scabbardPluginVersion = "0.1.0"

    const val rxJava2Version = "2.2.10"
    const val rxJava2AndroidVersion = "2.1.1"

    const val dagger2Version = "2.27"
    const val dagger2InjectAssistedVersion = "0.5.2"

    const val supportLibraryVersion = "1.1.0"
    const val cardViewVersion = "1.0.0"
    const val swipeRefreshLayoutVersion = "1.0.0"

    const val constraintLayoutVersion = "1.1.3"

    const val glideVersion = "4.11.0"

    const val ciceroneVersion = "5.1.0"
    const val moxyVersion = "2.1.1"

    const val retrofit2Version = "2.3.0"
    const val okhttp3Version = "3.9.1"
    const val okioVersion = "2.2.2" // should be max from okHttp3

    const val gsonVersion = "2.8.6"

    const val binaryPrefsVersion = "1.0.1"

    const val chuckerVersion = "3.1.2"
    const val leakCanaryVersion = "2.4"
    const val stethoVersion = "1.5.1"

    const val jUnitVersion = "4.12"
}