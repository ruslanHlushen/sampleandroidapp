package com.ruslan.hlushan.ui.impl.tools.di.impl

import android.view.View
import com.ruslan.hlushan.ui.api.utils.ViewModifier

internal class ViewModifierNoOpImpl : ViewModifier {

    override fun modify(view: View): View = view
}