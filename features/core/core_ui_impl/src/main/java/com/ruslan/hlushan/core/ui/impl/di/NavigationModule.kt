package com.ruslan.hlushan.core.ui.impl.di

import com.ruslan.hlushan.ui.api.router.FlowCiceronesHolder
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import javax.inject.Singleton

@Module
internal object NavigationModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideAppCicerone(): Cicerone<Router> = Cicerone.create()

    @JvmStatic
    @Provides
    @Singleton
    fun provideFlowCiceronesHolder(): FlowCiceronesHolder = FlowCiceronesHolder()
}