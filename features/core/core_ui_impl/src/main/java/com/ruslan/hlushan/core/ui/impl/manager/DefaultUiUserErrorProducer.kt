package com.ruslan.hlushan.core.ui.impl.manager

import com.ruslan.hlushan.core.api.exceptions.NetworkException
import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.api.managers.SimpleUserErrorMapper
import javax.inject.Inject

internal class DefaultUiUserErrorProducer @Inject constructor(
        private val resourceManager: ResourceManager
) : SimpleUserErrorMapper {

    @SuppressWarnings("UseIfInsteadOfWhen")
    override fun produceUserMessage(error: Throwable): String? =
            when (error) {
                is NetworkException -> resourceManager.getString(com.ruslan.hlushan.ui.api.R.string.error_internet_connection)
                else                -> null
            }
}