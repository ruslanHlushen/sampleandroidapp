package com.ruslan.hlushan.core.ui.impl.di

import com.ruslan.hlushan.core.api.di.CoreProvider
import com.ruslan.hlushan.core.api.managers.SimpleUserErrorMapper
import com.ruslan.hlushan.ui.impl.tools.di.UiToolsModule
import com.ruslan.hlushan.ui.api.di.UiCoreProvider
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [
    NavigationModule::class,
    CompositeUserErrorProducerProvider::class,
    UiToolsModule::class
], dependencies = [
    CoreProvider::class
])
@Singleton
interface UiCoreImplExportComponent : UiCoreProvider {

    @Component.Factory
    interface Factory {
        fun create(
                @BindsInstance external: List<SimpleUserErrorMapper>,
                coreProvider: CoreProvider
        ): UiCoreImplExportComponent
    }

    object Initializer {

        fun init(
                external: List<SimpleUserErrorMapper>,
                coreProvider: CoreProvider
        ): UiCoreImplExportComponent =
                DaggerUiCoreImplExportComponent.factory()
                        .create(
                                external = external,
                                coreProvider = coreProvider
                        )
    }
}