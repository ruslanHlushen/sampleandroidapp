package com.ruslan.hlushan.ui.impl.tools.di

import com.ruslan.hlushan.core.ui.tools.di.DebugUiToolsModule
import dagger.Module

@Module(includes = [DebugUiToolsModule::class])
internal object UiToolsModule