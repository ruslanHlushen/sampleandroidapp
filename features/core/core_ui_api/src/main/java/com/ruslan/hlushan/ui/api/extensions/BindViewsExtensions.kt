package com.ruslan.hlushan.ui.api.extensions

import android.app.Activity
import androidx.annotation.IdRes
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.ruslan.hlushan.extensions.lazyUnsafe

fun <V : View?> RecyclerView.ViewHolder.bindViewById(@IdRes viewResId: Int): Lazy<V> = lazyUnsafe { itemView.findViewById<V>(viewResId) }

fun <V : View?> Activity.bindViewById(@IdRes viewResId: Int): Lazy<V> = lazyUnsafe { findViewById<V>(viewResId) }