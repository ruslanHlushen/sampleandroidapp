package com.ruslan.hlushan.ui.api.manager

import com.ruslan.hlushan.core.api.managers.SimpleUserErrorMapper

class CompositeUserErrorMapper(
        private val simpleProducers: List<SimpleUserErrorMapper>
) {

    fun produceUserMessage(error: Throwable): String =
            (simpleProducers
                     .asSequence()
                     .map { producer -> producer.produceUserMessage(error) }
                     .filterNotNull()
                     .firstOrNull()
             ?: error.message.orEmpty())
}