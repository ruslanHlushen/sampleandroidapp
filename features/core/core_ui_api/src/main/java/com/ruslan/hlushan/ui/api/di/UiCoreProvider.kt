package com.ruslan.hlushan.ui.api.di

import com.ruslan.hlushan.ui.api.utils.ViewModifier
import com.ruslan.hlushan.ui.api.manager.CompositeUserErrorMapper
import com.ruslan.hlushan.ui.api.router.FlowCiceronesHolder
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

interface UiCoreProvider {

    fun provideAppCicerone(): Cicerone<Router>

    fun provideFlowCiceronesHolder(): FlowCiceronesHolder

    fun provideViewModifier(): ViewModifier

    fun provideCompositeUserErrorMapper(): CompositeUserErrorMapper
}