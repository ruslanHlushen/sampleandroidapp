package com.ruslan.hlushan.ui.api.router

import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

class FlowCiceronesHolder {

    private val flowCicerones: MutableMap<String, Cicerone<FlowRouter>> = mutableMapOf()

    fun getOrCreate(name: String, parentRouter: Router): Cicerone<FlowRouter> {
        val storedValue = flowCicerones[name]

        return if (storedValue != null) {
            storedValue
        } else {
            val newCicerone = Cicerone.create(FlowRouter(parentRouter))
            flowCicerones[name] = newCicerone
            return newCicerone
        }
    }

    fun clear(name: String) = flowCicerones.remove(name)
}