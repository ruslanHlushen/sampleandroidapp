package com.ruslan.hlushan.ui.api.utils

import android.os.Handler
import java.util.concurrent.TimeUnit

class LockableHandler(defaultIsLocked: Boolean) {

    private val handler = Handler()

    private var isLocked: Boolean = defaultIsLocked

    fun post(action: Runnable) = executeIfUnlocked {
        handler.post(action)
    }

    fun removeDelayed(action: Runnable) = handler.removeCallbacks(action)

    fun postDelayed(delay: Long, delayTimeUnit: TimeUnit, action: Runnable) = executeIfUnlocked {
        handler.postDelayed(action, delayTimeUnit.toMillis(delay))
    }

    fun unlock() {
        isLocked = false
    }

    fun lockAndClearDelayed() {
        isLocked = true
        handler.removeCallbacksAndMessages(null)
    }

    private inline fun executeIfUnlocked(block: () -> Unit) {
        if (!isLocked) {
            block()
        }
    }
}