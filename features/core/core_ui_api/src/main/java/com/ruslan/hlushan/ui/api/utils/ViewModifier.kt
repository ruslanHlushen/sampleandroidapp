package com.ruslan.hlushan.ui.api.utils

import android.view.View

interface ViewModifier {

    fun modify(view: View): View
}