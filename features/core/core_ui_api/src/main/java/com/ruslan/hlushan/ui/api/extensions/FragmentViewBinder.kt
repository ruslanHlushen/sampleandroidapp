package com.ruslan.hlushan.ui.api.extensions

import android.view.View
import androidx.annotation.IdRes
import com.ruslan.hlushan.ui.api.presentation.view.fragment.BaseFragment
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

internal class FragmentViewBinder<out V : View?>(@IdRes private val viewResId: Int) : ReadOnlyProperty<BaseFragment, V?> {

    override fun getValue(thisRef: BaseFragment, property: KProperty<*>): V? {
        var value = thisRef.viewsMap[viewResId]

        if (value == null) {
            thisRef.view?.findViewById<View?>(viewResId)?.let { view ->
                value = view
                thisRef.viewsMap[viewResId] = view
            }
        }

        @Suppress("UNCHECKED_CAST")
        return (value as? V?)
    }
}