package com.ruslan.hlushan.ui.api.presentation.view.fragment

import androidx.annotation.ContentView
import androidx.annotation.LayoutRes
import com.ruslan.hlushan.ui.api.presentation.presenter.BasePresenter
import com.ruslan.hlushan.ui.api.presentation.view.IBaseView

abstract class BaseMvpFragment<P : BasePresenter<*>>
@ContentView
constructor(@LayoutRes layoutResId: Int) : BaseFragment(layoutResId), IBaseView {

    protected abstract fun providePresenter(): P

    protected abstract val presenter: P
}