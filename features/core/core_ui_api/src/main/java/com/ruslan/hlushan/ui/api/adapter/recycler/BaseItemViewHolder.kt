package com.ruslan.hlushan.ui.api.adapter.recycler

import androidx.annotation.CallSuper
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import com.ruslan.hlushan.core.api.utils.UiMainThread

abstract class BaseItemViewHolder<Id : Any, RI : RecyclerItem<Id>>(itemView: View) : RecyclerView.ViewHolder(itemView) {

    @UiMainThread
    protected var recyclerItem: RI? = null

    @UiMainThread
    @CallSuper
    open fun onViewAttachedToWindow() = Unit

    @UiMainThread
    @CallSuper
    open fun onViewDetachedFromWindow() = Unit

    @UiMainThread
    @CallSuper
    open fun onViewRecycled() {
        recyclerItem = null
    }

    @UiMainThread
    @CallSuper
    open fun onBindView(item: RI) {
        recyclerItem = item
    }
}