package com.ruslan.hlushan.ui.api.presentation.view.activity

import android.content.Intent
import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.annotation.StyleRes
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.ui.api.R
import com.ruslan.hlushan.ui.api.presentation.view.fragment.BaseFragment
import com.ruslan.hlushan.ui.api.presentation.view.IBaseView
import com.ruslan.hlushan.ui.api.utils.ViewModifier
import com.ruslan.hlushan.ui.api.insets.applyWindowTransparencyAfterSetContentView
import com.ruslan.hlushan.ui.api.insets.applyWindowTransparencyBeforeSetContentView
import com.ruslan.hlushan.ui.api.presentation.presenter.BasePresenter
import moxy.MvpAppCompatActivity
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import javax.inject.Inject

abstract class BaseAppActivity<P : BasePresenter<*>> : MvpAppCompatActivity(), IBaseView {

    @Inject
    lateinit var appCicerone: Cicerone<Router>
    @Inject
    lateinit var viewModifier: ViewModifier

    @Inject
    protected lateinit var appLogger: AppLogger

    protected abstract fun providePresenter(): P

    protected abstract val presenter: P

    protected abstract fun initDagger2()

    protected abstract fun createNavigator(): Navigator

    @get:LayoutRes
    protected open val layoutResId: Int get() = R.layout.app_layout_container

    @get:IdRes
    protected open val appContainerResId: Int get() = R.id.app_container

    @get:StyleRes
    protected abstract val appThemeReId: Int

    internal var instanceStateSaved: Boolean = false
        private set

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(appThemeReId)
        initDagger2()
        this.applyWindowTransparencyBeforeSetContentView()
        appLogger.log(this, "before super.onCreate(Bundle?) savedInstanceState = $savedInstanceState")
        super.onCreate(savedInstanceState)
        appLogger.log(this, "after super.onCreate(Bundle?)")
//        appActivitiesSettings.changeLangIfNeeded(this)
        appLogger.log(this, "before setContentView(int)")
        setContentView(viewModifier.modify(layoutInflater.inflate(layoutResId, null)))
        this.applyWindowTransparencyAfterSetContentView(findViewById(appContainerResId))
        appLogger.log(this, "after setContentView(int)")
        if (savedInstanceState == null) {
            setUpFirstAppScreen()
        }
    }

    protected abstract fun setUpFirstAppScreen()

    private val currentFragment: BaseFragment?
        get() = supportFragmentManager.findFragmentById(appContainerResId) as? BaseFragment

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }

    @CallSuper
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        currentFragment?.onNewIntent(intent)
    }

    @CallSuper
    override fun onRestart() {
        appLogger.log(this)
        super.onRestart()
    }

    @CallSuper
    override fun onStart() {
        appLogger.log(this)
        super.onStart()
    }

    @CallSuper
    override fun onResumeFragments() {
        appLogger.log(this)
        super.onResumeFragments()
        instanceStateSaved = false
        appCicerone.navigatorHolder.setNavigator(createNavigator())
    }

    @CallSuper
    override fun onPause() {
        appLogger.log(this)
        appCicerone.navigatorHolder.removeNavigator()
        super.onPause()
    }

    @CallSuper
    override fun onStop() {
        appLogger.log(this)
        super.onStop()
    }

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        appLogger.log(this)
        super.onSaveInstanceState(outState)
        instanceStateSaved = true
    }

    @CallSuper
    override fun onDestroy() {
        appLogger.log(this)
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        appLogger.log(this)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        appLogger.log(this)
        super.onActivityResult(requestCode, resultCode, data)
    }
}