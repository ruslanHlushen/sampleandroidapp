package com.ruslan.hlushan.ui.api.presentation.view.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import com.google.android.material.snackbar.Snackbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.ContentView
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.api.utils.UiMainThread
import com.ruslan.hlushan.extensions.executeHideKeyboard
import com.ruslan.hlushan.extensions.showSnackBar
import com.ruslan.hlushan.ui.api.presentation.view.activity.BaseAppActivity
import com.ruslan.hlushan.extensions.ViewLambdaListener
import com.ruslan.hlushan.ui.api.extensions.FragmentViewBinder
import com.ruslan.hlushan.ui.api.manager.CompositeUserErrorMapper
import com.ruslan.hlushan.ui.api.utils.LockableHandler
import moxy.MvpAppCompatFragment
import ru.terrakok.cicerone.Router
import javax.inject.Inject
import kotlin.properties.ReadOnlyProperty

@SuppressWarnings("TooManyFunctions")
abstract class BaseFragment
@ContentView
constructor(
        @LayoutRes layoutResId: Int
) : MvpAppCompatFragment(layoutResId) {

    @Inject
    protected lateinit var resourceManager: ResourceManager

    @Inject
    protected lateinit var appLogger: AppLogger

    @Inject
    protected lateinit var compositeUserErrorMapper: CompositeUserErrorMapper

    @Suppress("UnsafeCast")
    protected val parentRouter: Router
        get() = (((parentFragment as? BaseFlowFragment)?.flowCicerone
                  ?: (activity as BaseAppActivity<*>).appCicerone).router)

    private var snackBar: Snackbar? = null

    internal var instanceStateSaved: Boolean = false
        private set

    protected val viewsHandler = LockableHandler(defaultIsLocked = true)

    internal val viewsMap = mutableMapOf<@IdRes Int, View>()

    abstract fun injectDagger2()

    @CallSuper
    override fun onAttach(context: Context) {
        injectDagger2()
        appLogger.log(this)
        super.onAttach(context)
    }

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        appLogger.log(this)
        super.onCreate(savedInstanceState)
        savedInstanceState?.let { restoreFromSavedInstanceState(it) }
    }

    @CallSuper
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        appLogger.log(this)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    @CallSuper
    protected open fun restoreFromSavedInstanceState(savedInstanceState: Bundle) = appLogger.log(this)

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        appLogger.log(this)
        super.onViewCreated(view, savedInstanceState)
        viewsHandler.unlock()
    }

    @CallSuper
    open fun onNewIntent(intent: Intent?) = appLogger.log(this)

    @CallSuper
    override fun onStart() {
        appLogger.log(this)
//        activity?.let { appActivitiesSettings.checkLocaleAndRecreateIfNeeded(it) }
        super.onStart()
    }

    @CallSuper
    override fun onResume() {
        appLogger.log(this)
        super.onResume()
        instanceStateSaved = false
    }

    @CallSuper
    override fun onPause() {
        appLogger.log(this)
        hideKeyboard()
        super.onPause()
    }

    @CallSuper
    override fun onStop() {
        appLogger.log(this)
        super.onStop()
    }

    @CallSuper
    override fun onDestroyView() {
        appLogger.log(this)
        viewsHandler.lockAndClearDelayed()
        clearViews()
        super.onDestroyView()
    }

    @CallSuper
    override fun onSaveInstanceState(outState: Bundle) {
        appLogger.log(this)
        super.onSaveInstanceState(outState)
        instanceStateSaved = true
    }

    @CallSuper
    override fun onDestroy() {
        appLogger.log(this)
        clearViews()
        super.onDestroy()
        if (needCloseScope()) {
            onCloseScope()
        }
    }

    @CallSuper
    override fun onDetach() {
        appLogger.log(this)
        clearViews()
        super.onDetach()
    }

    open fun onBackPressed() = parentRouter.exit()

    protected open fun onCloseScope() = Unit

    @CallSuper
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        appLogger.log(this)
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    @CallSuper
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        appLogger.log(this)
        super.onActivityResult(requestCode, resultCode, data)
    }

    @UiMainThread
    fun hideKeyboard() {
        appLogger.log(this)
        activity?.executeHideKeyboard()
    }

    @UiMainThread
    fun showSystemMessage(text: String, longDuration: Boolean) {
        activity?.let { fragmentActivity ->
            Toast.makeText(fragmentActivity, text, if (longDuration) Toast.LENGTH_LONG else Toast.LENGTH_SHORT).show()
        }
    }

    @UiMainThread
    fun showSnackBar(
            message: String,
            actionText: String? = null,
            onActionListener: (() -> Unit)? = null,
            duration: Int =  Snackbar.LENGTH_INDEFINITE
    ) {
        appLogger.log(this)
        hideSnackBar()
        hideKeyboard()
        activity?.let { nonNullActivity ->
            val listener: View.OnClickListener? = onActionListener?.let { l -> ViewLambdaListener(l) }
            snackBar = view?.showSnackBar(message, actionText, listener, duration)
        }
    }

    @UiMainThread
    fun hideSnackBar() {
        appLogger.log(this)
        snackBar?.dismiss()
        snackBar = null
    }

    @UiMainThread
    fun showError(error: Throwable) = this.showSnackBar(
            message = compositeUserErrorMapper.produceUserMessage(error = error),
            actionText = getString(android.R.string.ok)
    )

    //This is android, baby!
    private fun isRealRemoving(): Boolean =
            (isRemoving && !instanceStateSaved) //because isRemoving == true for fragment in backstack on screen rotation
            || ((parentFragment as? BaseFragment)?.isRealRemoving() ?: false)

    //It will be valid only for 'onDestroy()' method
    private fun needCloseScope(): Boolean =
            when {
                (activity?.isChangingConfigurations == true) -> false
                (activity?.isFinishing == true)              -> true
                else                                         -> isRealRemoving()
            }

    private fun clearViews() {
        viewsMap.clear()
    }

    protected fun <V : View?> bindViewById(@IdRes viewResId: Int): ReadOnlyProperty<BaseFragment, V?> = FragmentViewBinder<V>(viewResId)
}