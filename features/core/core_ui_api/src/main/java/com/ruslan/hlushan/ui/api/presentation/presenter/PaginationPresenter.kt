package com.ruslan.hlushan.ui.api.presentation.presenter

import com.ruslan.hlushan.core.api.dto.PaginationResponse
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.api.managers.SchedulersManager
import com.ruslan.hlushan.core.api.utils.UiMainThread
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import ru.terrakok.cicerone.Router
import java.util.concurrent.TimeUnit
import com.ruslan.hlushan.extensions.exhaustive
import com.ruslan.hlushan.extensions.safetyDispose
import com.ruslan.hlushan.ui.api.adapter.recycler.RecyclerItem
import com.ruslan.hlushan.ui.api.presentation.view.IBaseView

private const val DEFAULT_ITEMS_LIMIT_PER_PAGE: Int = 20
private const val FILTER_UPDATE_DEBOUNCE_MILLIS: Long = 300

abstract class PaginationPresenter<F : Any, ItemId : Any, PageId : Any, V : IBaseView>(
    initFilter: F,
    resourceManager: ResourceManager,
    router: Router,
    appLogger: AppLogger,
    schedulersManager: SchedulersManager
) : BasePresenter<V>(resourceManager, router, appLogger, schedulersManager) {

    private val filterSubject = PublishSubject.create<F>()

    private var requestDisposable: Disposable? = null

    @UiMainThread
    protected var state: PaginationState<F, ItemId, PageId> = PaginationState.Empty(filter = initFilter)
        private set(newValue) {
            if (field != newValue) {
                field = newValue
                onStateUpdated()
            }
        }

    protected abstract fun loadData(nextId: PageId?, filter: F): Single<PaginationResponse<RecyclerItem<ItemId>, PageId>>

    protected abstract fun onStateUpdated()

    protected open val itemsLimitPerPage: Int get() = DEFAULT_ITEMS_LIMIT_PER_PAGE

    @UiMainThread
    protected open val totalItemCount: Int
        get() = state.items.size

    init {
        filterSubject
            .debounce(FILTER_UPDATE_DEBOUNCE_MILLIS, TimeUnit.MILLISECONDS)
            .distinctUntilChanged()
            .observeOn(schedulersManager.ui)
            .doOnNext { filter -> handleFilterUpdated(filter) }
            .subscribe(
                { action -> this.appLogger.log(this@PaginationPresenter, "action = $action") },
                { error -> this.appLogger.log(this@PaginationPresenter, "filterSubject", error) }
            )
            .joinToHard()
    }

    @UiMainThread
    fun retry() = loadMoreAction()

    @UiMainThread
    fun refresh() = handleAction(action = Action.UI.Refresh(state.filter))

    @UiMainThread
    fun onScrolled(lastVisibleItemPosition: Int) {
        @SuppressWarnings("MagicNumber")
        if ((lastVisibleItemPosition + (itemsLimitPerPage / 3) > totalItemCount)) {
            loadMoreAction()
        }
    }

    @UiMainThread
    protected fun updateFilter(newFilter: F) = filterSubject.onNext(newFilter)

    @UiMainThread
    private fun handleFilterUpdated(newFilter: F) = handleAction(action = Action.UI.Refresh(newFilter))

    @UiMainThread
    private fun loadMoreAction() = handleAction(action = Action.UI.LoadMore())

    @UiMainThread
    private fun handleAction(action: Action<F, ItemId, PageId>) {
        val result = reduceState(
            state = state,
            action = action
        )

        state = result.newState

        result.sideEffects.forEach { sideEffect ->
            when (sideEffect) {
                is SideEffect.LoadNext -> {
                    loadMore(result.newState.nextId, result.newState.filter)
                }
            }.exhaustive
        }
    }

    private fun loadMore(nextId: PageId?, filter: F) {
        requestDisposable?.safetyDispose()

        requestDisposable = loadData(nextId, filter)
            .map<Action<F, ItemId, PageId>> { response -> Action.Response.Success(response) }
            .onErrorReturn { error -> Action.Response.Error(error) }
            .observeOn(schedulersManager.ui)
            .doOnSuccess { action -> handleAction(action) }
            .subscribe(
                { action -> appLogger.log(this@PaginationPresenter, "request action = $action") },
                { error -> appLogger.log(this@PaginationPresenter, "request", error) }
            )

        requestDisposable?.joinToHard()
    }
}

private sealed class Action<out F : Any, ItemId : Any, out PageId : Any> {

    sealed class UI<out F : Any, ItemId : Any, out PageId : Any> : Action<F, ItemId, PageId>() {
        class LoadMore<out F : Any, ItemId : Any, out PageId : Any> : UI<F, ItemId, PageId>()
        data class Refresh<out F : Any, ItemId : Any, out PageId : Any>(val filter: F) : UI<F, ItemId, PageId>()
    }

    sealed class Response<out F : Any, ItemId : Any, out PageId : Any> : Action<F, ItemId, PageId>() {

        class Success<out F : Any, ItemId : Any, out PageId : Any>(val response: PaginationResponse<RecyclerItem<ItemId>, PageId>) : Response<F, ItemId, PageId>()

        class Error<out F : Any, ItemId : Any, out PageId : Any>(val error: Throwable) : Response<F, ItemId, PageId>()
    }
}

private sealed class SideEffect {
    class LoadNext : SideEffect()
}

private class ReduceResult<out F : Any, ItemId : Any, out PageId : Any>(
    val newState: PaginationState<F, ItemId, PageId>,
    vararg val sideEffects: SideEffect
)

private fun <F : Any, ItemId : Any, PageId : Any> reduceState(
    state: PaginationState<F, ItemId, PageId>,
    action: Action<F, ItemId, PageId>
): ReduceResult<F, ItemId, PageId> =
    when (state) {
        is PaginationState.Empty                     -> reduceStateEmpty(state, action)
        is PaginationState.EmptyLoading              -> reduceStateEmptyLoading(state, action)
        is PaginationState.EmptyWithError            -> reduceStateEmptyWithError(state, action)
        is PaginationState.PartiallyLoaded           -> reduceStatePartiallyLoaded(state, action)
        is PaginationState.PartiallyLoadedAndLoading -> reduceStatePartiallyLoadedAndLoading(state, action)
        is PaginationState.PartiallyLoadedWithError  -> reduceStatePartiallyLoadedWithError(state, action)
        is PaginationState.AllLoaded                 -> reduceStateAllLoaded(state, action)
    }

private fun <F : Any, ItemId : Any, PageId : Any> reduceStateEmpty(
    state: PaginationState.Empty<F, ItemId, PageId>,
    action: Action<F, ItemId, PageId>
): ReduceResult<F, ItemId, PageId> =
    when (action) {
        is Action.UI.Refresh     -> {
            ReduceResult(PaginationState.EmptyLoading(filter = action.filter), SideEffect.LoadNext())
        }
        is Action.UI.LoadMore    -> {
            ReduceResult(PaginationState.EmptyLoading(filter = state.filter), SideEffect.LoadNext())
        }
        is Action.Response.Success,
        is Action.Response.Error -> {
            ReduceResult(state)
        }
    }

private fun <F : Any, ItemId : Any, PageId : Any> reduceStateEmptyLoading(
    state: PaginationState.EmptyLoading<F, ItemId, PageId>,
    action: Action<F, ItemId, PageId>
): ReduceResult<F, ItemId, PageId> =
    when (action) {
        is Action.UI.Refresh       -> {
            ReduceResult(PaginationState.EmptyLoading(action.filter), SideEffect.LoadNext())
        }
        is Action.UI.LoadMore      -> {
            ReduceResult(state)
        }
        is Action.Response.Success -> {
            when (action.response) {
                is PaginationResponse.MiddlePage -> ReduceResult(PaginationState.PartiallyLoaded(action.response.result, filter = state.filter, nextId = action.response.nextId))
                is PaginationResponse.LastPage   -> ReduceResult(PaginationState.AllLoaded(action.response.result, filter = state.filter))
            }
        }
        is Action.Response.Error   -> {
            ReduceResult(PaginationState.EmptyWithError(filter = state.filter, error = action.error))
        }
    }

private fun <F : Any, ItemId : Any, PageId : Any> reduceStateEmptyWithError(
    state: PaginationState.EmptyWithError<F, ItemId, PageId>,
    action: Action<F, ItemId, PageId>
): ReduceResult<F, ItemId, PageId> =
    when (action) {
        is Action.UI.Refresh     -> {
            ReduceResult(PaginationState.EmptyLoading(filter = action.filter), SideEffect.LoadNext())
        }
        is Action.UI.LoadMore    -> {
            ReduceResult(PaginationState.EmptyLoading(filter = state.filter), SideEffect.LoadNext())
        }
        is Action.Response.Success,
        is Action.Response.Error -> {
            ReduceResult(state)
        }
    }

private fun <F : Any, ItemId : Any, PageId : Any> reduceStatePartiallyLoaded(
    state: PaginationState.PartiallyLoaded<F, ItemId, PageId>,
    action: Action<F, ItemId, PageId>
): ReduceResult<F, ItemId, PageId> =
    when (action) {
        is Action.UI.Refresh     -> {
            ReduceResult(PaginationState.EmptyLoading(filter = action.filter), SideEffect.LoadNext())
        }
        is Action.UI.LoadMore    -> {
            ReduceResult(PaginationState.PartiallyLoadedAndLoading(state.items, filter = state.filter, nextId = state.nextId), SideEffect.LoadNext())
        }
        is Action.Response.Success,
        is Action.Response.Error -> {
            ReduceResult(state)
        }
    }

private fun <F : Any, ItemId : Any, PageId : Any> reduceStatePartiallyLoadedAndLoading(
    state: PaginationState.PartiallyLoadedAndLoading<F, ItemId, PageId>,
    action: Action<F, ItemId, PageId>
): ReduceResult<F, ItemId, PageId> =
    when (action) {
        is Action.UI.Refresh       -> {
            ReduceResult(PaginationState.EmptyLoading(filter = action.filter), SideEffect.LoadNext())
        }
        is Action.UI.LoadMore      -> {
            ReduceResult(state)
        }
        is Action.Response.Success -> {
            when (action.response) {
                is PaginationResponse.MiddlePage -> ReduceResult(PaginationState.PartiallyLoaded((state.items + action.response.result), filter = state.filter, nextId = action.response.nextId))
                is PaginationResponse.LastPage   -> ReduceResult(PaginationState.AllLoaded((state.items + action.response.result), filter = state.filter))
            }
        }
        is Action.Response.Error   -> {
            ReduceResult(PaginationState.PartiallyLoadedWithError(state.items, filter = state.filter, nextId = state.nextId, error = action.error))
        }
    }

private fun <F : Any, ItemId : Any, PageId : Any> reduceStatePartiallyLoadedWithError(
    state: PaginationState.PartiallyLoadedWithError<F, ItemId, PageId>,
    action: Action<F, ItemId, PageId>
): ReduceResult<F, ItemId, PageId> =
    when (action) {
        is Action.UI.Refresh     -> {
            ReduceResult(PaginationState.EmptyLoading(filter = action.filter), SideEffect.LoadNext())
        }
        is Action.UI.LoadMore    -> {
            ReduceResult(PaginationState.PartiallyLoadedAndLoading(state.items, filter = state.filter, nextId = state.nextId), SideEffect.LoadNext())
        }
        is Action.Response.Success,
        is Action.Response.Error -> {
            ReduceResult(state)
        }
    }

private fun <F : Any, ItemId : Any, PageId : Any> reduceStateAllLoaded(
    state: PaginationState.AllLoaded<F, ItemId, PageId>,
    action: Action<F, ItemId, PageId>
): ReduceResult<F, ItemId, PageId> =
    when (action) {
        is Action.UI.Refresh     -> {
            ReduceResult(PaginationState.EmptyLoading(action.filter), SideEffect.LoadNext())
        }
        is Action.UI.LoadMore,
        is Action.Response.Success,
        is Action.Response.Error -> {
            ReduceResult(state)
        }
    }