package com.ruslan.hlushan.ui.api.presentation.view.fragment

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.ruslan.hlushan.extensions.lazyUnsafe
import com.ruslan.hlushan.ui.api.R
import com.ruslan.hlushan.ui.api.router.FlowCiceronesHolder
import com.ruslan.hlushan.ui.api.router.FlowRouter
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import javax.inject.Inject

abstract class BaseFlowFragment(
        @LayoutRes layoutResId: Int = R.layout.app_layout_container
) : BaseFragment(layoutResId) {

    private val currentFragment
        get() = childFragmentManager.findFragmentById(R.id.app_container) as? BaseFragment

    @Inject
    lateinit var flowCiceronesHolder: FlowCiceronesHolder

    protected abstract val flowName: String

    val flowCicerone: Cicerone<FlowRouter> by lazyUnsafe { flowCiceronesHolder.getOrCreate(flowName, parentRouter) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (childFragmentManager.fragments.isEmpty()) {
            openFirstFlowScreen()
        }
    }

    override fun onResume() {
        super.onResume()
        flowCicerone.navigatorHolder.setNavigator(createFlowNavigator())
    }

    override fun onPause() {
        flowCicerone.navigatorHolder.removeNavigator()
        super.onPause()
    }

    override fun onBackPressed() {
        currentFragment?.onBackPressed() ?: super.onBackPressed()
    }

    override fun onCloseScope() {
        super.onCloseScope()
        flowCiceronesHolder.clear(flowName)
    }

    protected abstract fun createFlowNavigator(): Navigator

    protected abstract fun openFirstFlowScreen()
}