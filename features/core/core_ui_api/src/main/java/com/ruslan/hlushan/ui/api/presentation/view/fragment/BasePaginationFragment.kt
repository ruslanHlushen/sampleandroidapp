package com.ruslan.hlushan.ui.api.presentation.view.fragment

import android.os.Bundle
import android.view.View
import androidx.annotation.ContentView
import androidx.annotation.LayoutRes
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.recyclerview.widget.RecyclerView
import com.ruslan.hlushan.core.api.utils.UiMainThread
import com.ruslan.hlushan.extensions.addPaginationScrollListener
import com.ruslan.hlushan.extensions.colorAttributeValue
import com.ruslan.hlushan.ui.api.adapter.recycler.BaseRecyclerAdapter
import com.ruslan.hlushan.ui.api.presentation.presenter.PaginationPresenter
import com.ruslan.hlushan.ui.api.presentation.view.IBaseView

abstract class BasePaginationFragment<P : PaginationPresenter<*, *, *, *>>
@ContentView
constructor(
        @LayoutRes layoutResId: Int
) : BaseMvpFragment<P>(layoutResId), IBaseView {

    protected abstract val recyclerViewItems: RecyclerView?
    protected abstract val swipeRefreshLayout: SwipeRefreshLayout?

    protected abstract val basePaginationRecyclerAdapter: BaseRecyclerAdapter<*, *>

    private val enableRefreshingRunnable: Runnable = Runnable {
        if (swipeRefreshLayout?.isRefreshing == false) {
            swipeRefreshLayout?.isRefreshing = true
        }
    }

    private val disableRefreshingRunnable: Runnable = Runnable {
        if (swipeRefreshLayout?.isRefreshing == true) {
            swipeRefreshLayout?.isRefreshing = false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewItems?.let { rec -> basePaginationRecyclerAdapter.setUpWithRecycler(rec) }
        recyclerViewItems?.addPaginationScrollListener(presenter::onScrolled)
        setRefreshListener()
    }

    override fun onDestroyView() {
        recyclerViewItems?.adapter = null
        super.onDestroyView()
    }

    @UiMainThread
    protected fun showSwipeProgress(show: Boolean) {
        if (show) {
            viewsHandler.removeDelayed(disableRefreshingRunnable)
            viewsHandler.post(enableRefreshingRunnable)
        } else {
            viewsHandler.removeDelayed(enableRefreshingRunnable)
            viewsHandler.post(disableRefreshingRunnable)
        }
    }

    private fun setRefreshListener() {
        activity?.let { activityNonNull ->
            swipeRefreshLayout?.setColorSchemeColors(activityNonNull.colorAttributeValue(com.google.android.material.R.attr.colorSecondary))
        }
        swipeRefreshLayout?.setOnRefreshListener { presenter.refresh() }
    }
}