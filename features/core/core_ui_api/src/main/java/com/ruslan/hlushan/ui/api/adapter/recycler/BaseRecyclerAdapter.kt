package com.ruslan.hlushan.ui.api.adapter.recycler

import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ruslan.hlushan.core.api.utils.UiMainThread
import com.ruslan.hlushan.extensions.clearAndAddAll
import com.ruslan.hlushan.extensions.indexOfOrNull

typealias OnItemClickListener<T> = (item: T) -> Unit

//TODO: think about async
@SuppressWarnings("TooManyFunctions")
abstract class BaseRecyclerAdapter<Id : Any, RI : RecyclerItem<Id>>(
        initItems: List<RI> = emptyList()
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items: MutableList<RI> = initItems.toMutableList()

    private var inflater: LayoutInflater? = null
    private val spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
        override fun getSpanSize(position: Int): Int = items[position].gridSpan
    }

    @UiMainThread
    open fun setUpWithRecycler(recyclerView: RecyclerView) =
            setUpRecyclerDefaults(
                    recyclerView = recyclerView,
                    layoutManager = LinearLayoutManager(recyclerView.context),
                    hasFixedSize = true
            )

    @UiMainThread
    protected fun setUpRecyclerDefaults(recyclerView: RecyclerView, layoutManager: RecyclerView.LayoutManager, hasFixedSize: Boolean) {
        recyclerView.layoutManager = layoutManager
        recyclerView.setHasFixedSize(hasFixedSize)
        recyclerView.adapter = this
    }

    @UiMainThread
    val realItemsCount: Int
        get() = itemCount

    @UiMainThread
    val lastRealItem: RI?
        get() = items.lastOrNull()

    @UiMainThread
    fun getItemsCopy(): List<RI> = items.toList()

    @UiMainThread
    fun getItem(position: Int): RI? = items.getOrNull(position)

    @UiMainThread
    fun insertAtPosition(item: RI, position: Int = items.size) {
        items.add(position, item)
        notifyItemInserted(position)
    }

    @UiMainThread
    fun setAll(newItems: List<RI>) {
        val oldItems = items.toList()
        val diffResult = DiffUtil.calculateDiff(DiffHelper(newItems = newItems, oldItems = oldItems))
        items.clearAndAddAll(newItems)
        diffResult.dispatchUpdatesTo(this)
    }

    @UiMainThread
    fun addAll(itemsToAdd: List<RI>) = setAll(items + itemsToAdd)

    @UiMainThread
    fun setAtPosition(item: RI, position: Int) {
        if (position in items.indices) {
            items[position] = item
            notifyItemChanged(position)
        }
    }

    @UiMainThread
    fun removeAtPosition(position: Int) {
        if (position in items.indices) {
            items.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    @UiMainThread
    fun removeItem(item: RI) {
        items.indexOfOrNull(item)?.let { validIndex -> removeAtPosition(validIndex) }
    }

    @UiMainThread
    fun clear() {
        val size = items.size
        items.clear()
        notifyItemRangeRemoved(0, size)
    }

    @UiMainThread
    fun moveTo(fromPosition: Int, toPosition: Int) {
        if (fromPosition in items.indices && toPosition in items.indices) {
            val item = items.removeAt(fromPosition)
            items.add(toPosition, item)
            notifyItemMoved(fromPosition, toPosition)
        }
    }

    /** @param type marked with [androidx.annotation.LayoutRes] because of [getItemViewType] and [onCreateViewHolder]*/
    protected abstract fun viewHolder(itemView: View, @LayoutRes type: Int): RecyclerView.ViewHolder

    /** @param type marked with [androidx.annotation.LayoutRes] because of [getItemViewType]*/
    override fun onCreateViewHolder(parent: ViewGroup, @LayoutRes type: Int): RecyclerView.ViewHolder =
            @Suppress("UnsafeCallOnNullableType")
            viewHolder(inflater!!.inflate(type, parent, false), type)

    @SuppressWarnings("UnsafeCast")
    @UiMainThread
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) =
            (viewHolder as BaseItemViewHolder<Id, RI>).onBindView(items[position])

    @UiMainThread
    override fun getItemCount(): Int = items.size

    @LayoutRes
    override fun getItemViewType(position: Int): Int = items[position].layoutResId

    @UiMainThread
    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)

        recyclerView.layoutManager?.let { setManager(it) }

        if (inflater == null) {
            this.inflater = LayoutInflater.from(recyclerView.context)
        }
    }

    override fun onFailedToRecycleView(holder: RecyclerView.ViewHolder): Boolean {
        //TODO: log this or maybe crash in debug
        return super.onFailedToRecycleView(holder)
    }

    @UiMainThread
    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        super.onViewAttachedToWindow(holder)
        @Suppress("UnsafeCast")
        (holder as BaseItemViewHolder<*, *>).onViewAttachedToWindow()
    }

    @UiMainThread
    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        @Suppress("UnsafeCast")
        (holder as BaseItemViewHolder<*, *>).onViewDetachedFromWindow()
    }

    @UiMainThread
    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        @Suppress("UnsafeCast")
        (holder as BaseItemViewHolder<*, *>).onViewRecycled()
    }

    @SuppressWarnings("")
    @UiMainThread
    private fun setManager(manager: RecyclerView.LayoutManager) {
        when (manager) {
            is GridLayoutManager          -> {
                manager.spanSizeLookup = spanSizeLookup
            }
            is StaggeredGridLayoutManager -> {
                manager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_MOVE_ITEMS_BETWEEN_SPANS
            }
        }
    }
}

private class DiffHelper<Id : Any>(
        private val newItems: List<RecyclerItem<Id>>,
        private val oldItems: List<RecyclerItem<Id>>
) : DiffUtil.Callback() {

    private val payload = Any()

    override fun getOldListSize(): Int = oldItems.size
    override fun getNewListSize(): Int = newItems.size

    //to avoid blinking
    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? = payload

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldItems[oldItemPosition].isTheSameItem(newItems[newItemPosition])

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean =
            oldItems[oldItemPosition].hasTheSameContent(newItems[newItemPosition])
}