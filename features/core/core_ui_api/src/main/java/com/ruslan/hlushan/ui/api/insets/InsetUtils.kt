package com.ruslan.hlushan.ui.api.insets

import android.app.Activity
import android.graphics.Color
import android.graphics.Rect
import android.os.Build
import android.view.View
import android.widget.FrameLayout
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

fun Activity.applyWindowTransparencyBeforeSetContentView() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
        window.apply {
            decorView.systemUiVisibility =
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                decorView.systemUiVisibility = decorView.systemUiVisibility or
                        View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
            }

            statusBarColor = Color.TRANSPARENT
            navigationBarColor = Color.TRANSPARENT
        }
    }
}

fun Activity.applyWindowTransparencyAfterSetContentView(container: FrameLayout) {
    container.doOnApplyWindowInsets { view, insets, initialPadding ->
        view.updatePadding(
                left = (initialPadding.left + insets.systemWindowInsetLeft),
                right = (initialPadding.right + insets.systemWindowInsetRight)
        )
        insets.replaceSystemWindowInsets(
                Rect(
                        0,
                        insets.systemWindowInsetTop,
                        0,
                        insets.systemWindowInsetBottom
                )
        )
    }
}

@SuppressWarnings("LongParameterList")
fun View.addSystemPadding(
        targetView: View = this,
        isConsumed: Boolean = false,
        left: Boolean = false,
        top: Boolean = false,
        right: Boolean = false,
        bottom: Boolean = false
) {
    doOnApplyWindowInsets { _, insets, initialPadding ->
        targetView.updatePadding(
                left = (initialPadding.left + +insets.systemWindowInsetLeft).takeIf { left },
                top = (initialPadding.top + insets.systemWindowInsetTop).takeIf { top },
                right = (initialPadding.right + insets.systemWindowInsetRight).takeIf { right },
                bottom = (initialPadding.bottom + insets.systemWindowInsetBottom).takeIf { bottom }
        )
        if (isConsumed) {
            insets.replaceSystemWindowInsets(
                    Rect(
                            if (left) 0 else insets.systemWindowInsetLeft,
                            if (top) 0 else insets.systemWindowInsetTop,
                            if (right) 0 else insets.systemWindowInsetRight,
                            if (bottom) 0 else insets.systemWindowInsetBottom
                    )
            )
        } else {
            insets
        }
    }
}

fun View.doOnApplyWindowInsets(block: (View, insets: WindowInsetsCompat, initialPadding: Rect) -> WindowInsetsCompat) {
    val initialPadding = recordInitialPaddingForView(this)
    ViewCompat.setOnApplyWindowInsetsListener(this) { v, insets ->
        block(v, insets, initialPadding)
    }
    requestApplyInsetsWhenAttached()
}

private fun recordInitialPaddingForView(view: View) =
        Rect(view.paddingLeft, view.paddingTop, view.paddingRight, view.paddingBottom)

private fun View.requestApplyInsetsWhenAttached() {
    if (isAttachedToWindow) {
        ViewCompat.requestApplyInsets(this)
    } else {
        addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
                v.removeOnAttachStateChangeListener(this)
                ViewCompat.requestApplyInsets(v)
            }

            override fun onViewDetachedFromWindow(v: View) = Unit
        })
    }
}

fun View.updatePadding(
        left: Int? = null,
        top: Int? = null,
        right: Int? = null,
        bottom: Int? = null
) = setPadding(
        (left ?: paddingLeft),
        (top ?: paddingTop),
        (right ?: paddingRight),
        (bottom ?: paddingBottom)
)