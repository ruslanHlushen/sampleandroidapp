package com.ruslan.hlushan.ui.api.adapter.recycler

import androidx.annotation.LayoutRes

interface RecyclerItem<Id : Any> {

    open val gridSpan: Int get() = 1

    @get:LayoutRes
    val layoutResId: Int

    val id: Id

    fun isTheSameItem(other: RecyclerItem<Id>): Boolean = (this.id == other.id)
    fun hasTheSameContent(other: RecyclerItem<Id>): Boolean = (this === other)
}