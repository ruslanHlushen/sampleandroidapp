package com.ruslan.hlushan.ui.api.presentation.presenter

import androidx.annotation.CallSuper
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.api.managers.SchedulersManager
import com.ruslan.hlushan.core.api.utils.UiMainThread
import com.ruslan.hlushan.ui.api.presentation.view.IBaseView
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import moxy.MvpPresenter
import ru.terrakok.cicerone.Router

abstract class BasePresenter<V : IBaseView>(
        protected val resourceManager: ResourceManager,
        protected val router: Router,
        protected val appLogger: AppLogger,
        protected val schedulersManager: SchedulersManager
) : MvpPresenter<V>() {

    private val liteCompositeDisposable = CompositeDisposable()
    private val hardCompositeDisposable = CompositeDisposable()

    @CallSuper
    @UiMainThread
    override fun attachView(view: V) {
        appLogger.log(this)
        super.attachView(view)
    }

    @CallSuper
    @UiMainThread
    override fun onFirstViewAttach() {
        appLogger.log(this)
        super.onFirstViewAttach()
    }

    @CallSuper
    @UiMainThread
    override fun detachView(view: V) {
        appLogger.log(this)
        clearLiteCompositeDisposable(isFinalClear = false)
        super.detachView(view)
    }

    @CallSuper
    @UiMainThread
    override fun destroyView(view: V) {
        appLogger.log(this)
        super.destroyView(view)
    }

    @CallSuper
    @UiMainThread
    override fun onDestroy() {
        appLogger.log(this)
        clearLiteCompositeDisposable(isFinalClear = true)
        clearHardCompositeDisposable()
        super.onDestroy()
    }

    private fun clearLiteCompositeDisposable(isFinalClear: Boolean) {
        if (isFinalClear) {
            liteCompositeDisposable.dispose()
        } else {
            liteCompositeDisposable.clear()
        }
    }

    private fun clearHardCompositeDisposable() {
        hardCompositeDisposable.dispose()
    }

    protected fun Disposable.joinToLite(): Boolean = liteCompositeDisposable.add(this)
    protected fun Disposable.joinToHard(): Boolean = hardCompositeDisposable.add(this)
}