package com.ruslan.hlushan.parsing.impl.di

import com.ruslan.hlushan.core.api.di.ParserManagerProvider
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ParsingManagerModule::class])
interface ParsingExportComponent : ConverterFactoryProvider, ParserManagerProvider {

    @Component.Factory
    interface Factory {
        fun create(): ParsingExportComponent
    }

    object Initializer {
        fun init(): ParsingExportComponent =
                DaggerParsingExportComponent.factory()
                        .create()
    }
}