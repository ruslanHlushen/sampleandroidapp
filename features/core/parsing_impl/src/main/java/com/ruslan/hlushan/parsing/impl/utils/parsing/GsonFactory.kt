package com.ruslan.hlushan.parsing.impl.utils.parsing

import com.google.gson.Gson
import com.google.gson.GsonBuilder

internal fun createGson(): Gson =
    GsonBuilder()
        .setLenient()
        .create()