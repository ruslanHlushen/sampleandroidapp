package com.ruslan.hlushan.parsing.impl.di

import com.google.gson.Gson
import com.ruslan.hlushan.core.api.managers.ParserManager
import com.ruslan.hlushan.parsing.impl.mvp.model.manager.ParserManagerImpl
import com.ruslan.hlushan.parsing.impl.utils.parsing.createGson
import dagger.Module
import dagger.Provides
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
internal class ParsingManagerModule {

    @Singleton
    @Provides
    fun provideGson(): Gson = createGson()

    @Singleton
    @Provides
    fun provideConverterFactory(gson: Gson): Converter.Factory = GsonConverterFactory.create(gson)

    @Singleton
    @Provides
    fun provideParserManager(parserManagerImpl: ParserManagerImpl): ParserManager = parserManagerImpl
}