package com.ruslan.hlushan.parsing.impl.di

import retrofit2.Converter

interface ConverterFactoryProvider {

    fun provideConverterFactory(): Converter.Factory
}