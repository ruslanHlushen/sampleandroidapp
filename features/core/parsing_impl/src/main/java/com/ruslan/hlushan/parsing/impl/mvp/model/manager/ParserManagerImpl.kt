package com.ruslan.hlushan.parsing.impl.mvp.model.manager

import com.google.gson.Gson
import com.ruslan.hlushan.core.api.managers.ParserManager
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import javax.inject.Inject

internal class ParserManagerImpl @Inject constructor(private val gson: Gson) : ParserManager {

    @Throws(Exception::class)
    override fun <T> fromJsonListOf(json: String?, classOfT: Class<T>): List<T>? =
            gson.fromJson<List<T>>(json, ListOfSomething<T>(classOfT))

    @Throws(Exception::class)
    override fun <T> fromJson(json: String?, classOfT: Class<T>): T? =
            gson.fromJson<T>(json, classOfT)
}

private class ListOfSomething<T>(private val wrapped: Class<T>) : ParameterizedType {
    override fun getRawType(): Type = List::class.java
    override fun getOwnerType(): Type? = null
    override fun getActualTypeArguments(): Array<Type> = arrayOf<Type>(wrapped)
}