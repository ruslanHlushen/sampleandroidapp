package com.ruslan.hlushan.network

import android.content.Context
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import com.ruslan.hlushan.core.api.utils.NetworkConfig
import com.ruslan.hlushan.network.interceptor.DefaultNetworkErrorsInterceptor
import com.ruslan.hlushan.network.tools.addDebugTools
import okhttp3.Cache
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.internal.tls.OkHostnameVerifier
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.io.File
import java.net.URI
import java.util.concurrent.TimeUnit

object NetworkBuildHelper {

    fun provideRetrofitBuilder(
        converterFactory: Converter.Factory,
        okHttpClient: OkHttpClient
    ): Retrofit.Builder =
        Retrofit.Builder()
            .addConverterFactory(converterFactory)
            .addCallAdapterFactory(provideRxJava2CallAdapterFactory())
            .client(okHttpClient)

    @SuppressWarnings("LongParameterList")
    fun provideOkHttpClientBuilder(
        initAppConfig: InitAppConfig,
        networkConfig: NetworkConfig,
        baseUrl: String,
        cacheFolderName: String,
        appLogger: AppLogger,
        context: Context
    ): OkHttpClient.Builder {

        val okBuilder = OkHttpClient.Builder()

        okBuilder.connectTimeout(networkConfig.connectTimeoutSeconds, TimeUnit.SECONDS)
        okBuilder.readTimeout(networkConfig.readTimeoutSeconds, TimeUnit.SECONDS)
        okBuilder.writeTimeout(networkConfig.readTimeoutSeconds, TimeUnit.SECONDS)

        okBuilder.addDebugTools(networkConfig, appLogger, context)

//        okBuilder.addInterceptor(GzipRequestInterceptor())

        okBuilder.addInterceptor(DefaultNetworkErrorsInterceptor())

        okBuilder.retryOnConnectionFailure(true)

        okBuilder.cache(provideCache(initAppConfig, networkConfig, cacheFolderName))

        val dispatcher = Dispatcher().apply {
            maxRequests = networkConfig.maxRequests
            maxRequestsPerHost = networkConfig.maxRequestsPerHost
        }
        okBuilder.dispatcher(dispatcher)

        val hostNameFromBaseUrl = extractHostName(baseUrl)
        okBuilder.hostnameVerifier { hostname, session ->
            OkHostnameVerifier.INSTANCE.verify(hostNameFromBaseUrl, session)
        }

        return okBuilder
    }

    private fun provideCache(
        initAppConfig: InitAppConfig,
        networkConfig: NetworkConfig,
        cacheFolderName: String
    ): Cache {
        val httpCacheDirectory = File(initAppConfig.fileLogsFolder, "responses/$cacheFolderName")
        return Cache(httpCacheDirectory, networkConfig.cacheSize)
    }

    private fun provideRxJava2CallAdapterFactory() = RxJava2CallAdapterFactory.create()
}

private fun extractHostName(baseUrl: String): String = URI(baseUrl).host