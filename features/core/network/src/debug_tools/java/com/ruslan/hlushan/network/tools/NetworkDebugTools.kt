package com.ruslan.hlushan.network.tools

import android.content.Context
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.utils.NetworkConfig
import okhttp3.OkHttpClient

internal fun OkHttpClient.Builder.addDebugTools(networkConfig: NetworkConfig, appLogger: AppLogger, context: Context) {
    provideDebugInterceptors(networkConfig, appLogger, context).forEach { debugInterceptor ->
        this.addInterceptor(debugInterceptor)
    }
}