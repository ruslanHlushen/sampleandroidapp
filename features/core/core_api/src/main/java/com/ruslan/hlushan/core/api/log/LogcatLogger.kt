package com.ruslan.hlushan.core.api.log

interface LogcatLogger {

    fun log(tag: String, logMessage: String)

    fun log(tag: String, logMessage: String?, error: Throwable?)
}