package com.ruslan.hlushan.core.api.utils

enum class ThemeMode {
    NIGHT,
    LIGHT,
    SAVE_BATTERY,
    SYSTEM_DEFAULT
}