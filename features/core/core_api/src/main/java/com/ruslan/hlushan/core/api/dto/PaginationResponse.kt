package com.ruslan.hlushan.core.api.dto

sealed class PaginationResponse<out T : Any, out Id : Any> {

    companion object {
        fun <T : Any, Id : Any> create(result: List<T>, nextId: Id?): PaginationResponse<T, Id> =
            if (nextId != null) {
                MiddlePage<T, Id>(result, nextId)
            } else {
                LastPage<T, Id>(result)
            }
    }

    abstract val result: List<T>
    abstract val nextId: Id?

    fun <R : Any> map(transform: (T) -> R): PaginationResponse<R, Id> =
        create<R, Id>(
            result = this.result.map(transform),
            nextId = this.nextId
        )

    fun <R : Any> mapNotNull(transform: (T) -> R?): PaginationResponse<R, Id> =
        create<R, Id>(
            result = this.result.mapNotNull(transform),
            nextId = this.nextId
        )

    data class LastPage<out T : Any, out Id : Any>(
        override val result: List<T>
    ) : PaginationResponse<T, Id>() {
        override val nextId: Id? get() = null
    }

    data class MiddlePage<out T : Any, out Id : Any>(
        override val result: List<T>,
        override val nextId: Id
    ) : PaginationResponse<T, Id>()
}