package com.ruslan.hlushan.core.api.utils

import java.io.File

data class InitAppConfig(
        val versionCode: Int,
        val versionName: String,
        val appTag: String,
        val isLogcatEnabled: Boolean,
        val isFileLogEnabled: Boolean,
        val fileLogsFolder: File
)