package com.ruslan.hlushan.core.api.exceptions

class NetworkException(throwable: Throwable) : Exception(throwable)