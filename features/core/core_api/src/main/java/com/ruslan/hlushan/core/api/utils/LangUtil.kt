package com.ruslan.hlushan.core.api.utils

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import java.util.Locale

var Configuration.currentLocale: Locale
    get() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        locales.get(0)
    } else {
        locale
    }
    set(newValue) {
        @Suppress("ObsoleteSdkInt")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setLocale(newValue)
        } else {
            locale = newValue
        }
    }

fun Context.wrapContextWithNewLanguage(currentAppLangNotFullCode: String): Context {
    val neededAppLocale = Locale(currentAppLangNotFullCode)
    Locale.setDefault(neededAppLocale)

    val res = resources
    val newConfig = Configuration(res.configuration)

    newConfig.currentLocale = neededAppLocale

    @Suppress("ObsoleteSdkInt")
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        createConfigurationContext(newConfig)
    } else {
        res.updateConfiguration(newConfig, res.displayMetrics)
        this
    }
}