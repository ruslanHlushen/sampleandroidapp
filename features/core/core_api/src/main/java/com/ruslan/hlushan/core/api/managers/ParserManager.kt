package com.ruslan.hlushan.core.api.managers

interface ParserManager {

    @Throws(Exception::class)
    fun <T> fromJsonListOf(json: String?, classOfT: Class<T>): List<T>?

    @Throws(Exception::class)
    fun <T> fromJson(json: String?, classOfT: Class<T>): T?
}