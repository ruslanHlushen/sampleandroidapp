package com.ruslan.hlushan.core.api.log

interface ErrorLogger {

    fun logError(throwable: Throwable?)
}