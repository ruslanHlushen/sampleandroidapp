package com.ruslan.hlushan.core.api.managers

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

interface ResourceManager {

    fun getString(@StringRes strResId: Int): String

    fun getString(@StringRes strResId: Int, vararg formatArgs: Any): String

    fun getNonTranslatableString(@StringRes strResId: Int): String

    fun getStringResourceByName(stringResName: String): String

    @DrawableRes
    fun getDrawableResourceIdByName(drawableResName: String): Int?
}