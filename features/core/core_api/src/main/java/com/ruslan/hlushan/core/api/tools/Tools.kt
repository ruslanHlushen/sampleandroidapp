package com.ruslan.hlushan.core.api.tools

interface LeakCanaryTool {
    var enabled: Boolean
    fun openScreen()
}

interface ChuckTool {
    fun logError(tag: String, error: Throwable)
    fun openScreen()
}