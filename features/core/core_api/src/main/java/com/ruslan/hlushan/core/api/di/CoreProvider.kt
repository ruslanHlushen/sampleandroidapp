package com.ruslan.hlushan.core.api.di

import android.content.Context
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.log.ErrorLogger
import com.ruslan.hlushan.core.api.managers.ParserManager
import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.api.managers.SchedulersManager
import com.ruslan.hlushan.core.api.utils.InitAppConfig

interface CoreProvider : ManagersProvider,
                         LoggersProvider,
                         SchedulersProvider,
                         AppContextProvider,
                         InitAppConfigProvider,
                         //tools
                         LeakCanaryToolProvider,
                         ChuckToolProvider

interface InitAppConfigProvider {

    fun provideInitAppConfig(): InitAppConfig
}

interface AppContextProvider {

    fun provideAppContext(): Context
}

interface ParserManagerProvider {

    fun provideParserManager(): ParserManager
}

interface LoggersProvider {

    fun provideAppLogger(): AppLogger

    fun provideErrorLogger(): ErrorLogger
}

interface ManagersProvider {

    fun provideResourceManager(): ResourceManager
}

interface SchedulersProvider {

    fun provideSchedulersManager(): SchedulersManager
}