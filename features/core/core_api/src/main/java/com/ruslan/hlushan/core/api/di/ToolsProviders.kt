package com.ruslan.hlushan.core.api.di

import com.ruslan.hlushan.core.api.tools.ChuckTool
import com.ruslan.hlushan.core.api.tools.LeakCanaryTool

interface LeakCanaryToolProvider {
    fun provideLeakCanaryTool(): LeakCanaryTool
}

interface ChuckToolProvider {
    fun provideChuckTool(): ChuckTool
}