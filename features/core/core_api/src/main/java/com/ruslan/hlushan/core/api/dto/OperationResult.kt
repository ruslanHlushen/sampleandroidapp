package com.ruslan.hlushan.core.api.dto

sealed class OperationResult<out S : Any?, out E : Any?> {

    data class Success<out S : Any?>(val result: S) : OperationResult<S, Nothing>()

    data class Error<out E : Any?>(val result: E) : OperationResult<Nothing, E>()
}

fun <S : Any?> OperationResult<S, Throwable>.getOrThrow(): S =
    when (this) {
        is OperationResult.Success -> this.result
        is OperationResult.Error -> throw this.result
    }