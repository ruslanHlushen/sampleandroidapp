plugins {
    id("com.android.library")
}

apply(from = extra["gradle_support_base_android"])
apply(from = extra["gradle_support_build_variants"])

val coreApiLibraryVersionName = "1"
val coreApiLibraryVersionCode = 1
val proguardRulesFile = "proguard-rules-core_api.pro"

android {
    defaultConfig {
        versionCode = coreApiLibraryVersionCode
        versionName = coreApiLibraryVersionName

        consumerProguardFiles(proguardRulesFile)
    }

    buildTypes {
        getByName(BuildTypes.release) {
            isMinifyEnabled = false
        }
    }

    resourcePrefix("core_api_")
}

dependencies {
    compileOnly(project(":extensions"))

    compileOnly(Deps.supportAnnotations)

    compileOnly(Deps.rxJava2)

    compileOnly(Deps.dagger2)
}