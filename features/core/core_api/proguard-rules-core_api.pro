-repackageclasses
-renamesourcefileattribute SourceFile

# Kotlin: This rule will help you to keep your annotation classes and it won't warn for reflection classes.
-dontwarn kotlin.**
-dontwarn kotlin.reflect.jvm.internal.**
-keep class kotlin.reflect.jvm.internal.** { *; }

# Kotlin: The consolidated rule for Kotlin android project.
-keep class kotlin.** { *; }
-keep class kotlin.Metadata { *; }
-keepclassmembers class **$WhenMappings {
    <fields>;
}
-keepclassmembers class kotlin.Metadata {
    public <methods>;
}
-assumenosideeffects class kotlin.jvm.internal.Intrinsics {
    static void checkParameterIsNotNull(java.lang.Object, java.lang.String);
}

#----------------- Android: START -----------------#
-keepclassmembers class * implements android.os.Parcelable {
      public static final android.os.Parcelable$Creator *;
}
-keep class org.parceler.Parceler$$Parcels
-dontwarn java.lang.invoke.*

-assumenosideeffects class android.util.Log {
    public static boolean isLoggable(java.lang.String, int);
    public static int v(...);
    public static int i(...);
    public static int w(...);
    public static int d(...);
    public static int e(...);
}

# Used by serializable classes
-keepnames class * implements java.io.Serializable

-keepclassmembers class * implements java.io.Serializable {
    static final long serialVersionUID;
    private static final java.io.ObjectStreamField[] serialPersistentFields;
    !static !transient <fields>;
    !private <fields>;
    !private <methods>;
    private void writeObject(java.io.ObjectOutputStream);
    private void readObject(java.io.ObjectInputStream);
    java.lang.Object writeReplace();
    java.lang.Object readResolve();
}

# Okio
-keep class sun.misc.Unsafe { *; }
-dontwarn sun.misc.Unsafe
-dontwarn java.nio.file.*
-dontwarn org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement
-dontwarn okio.**

##---------------Begin: proguard configuration for android  ----------
-keep class com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**

-dontwarn com.google.android.material.**
-keep class com.google.android.material.** { *; }

-dontwarn androidx.**
-keep class androidx.** { *; }
-keep interface androidx.** { *; }

-keepclassmembers class fqcn.of.javascript.interface.for.webview {
   public *;
}

-assumevalues class android.os.Build$VERSION {
    int SDK_INT return 21..2147483647;
}

##-----
-keep public class * extends android.app.Activity
-keep public class * extends android.app.Application
-keep public class * extends android.app.Service
-keep public class * extends android.content.BroadcastReceiver
-keep public class * extends android.content.ContentProvider
-keep public class * extends android.app.backup.BackupAgentHelper
-keep public class * extends android.preference.Preference
-keep public class com.android.vending.licensing.ILicensingService
-keepclasseswithmembernames class * { native <methods>; }
-keepclasseswithmembers class * { public <init>(android.content.Context, android.util.AttributeSet); }
-keepclasseswithmembers class * { public <init>(android.content.Context, android.util.AttributeSet, int); }
-keepclassmembers class * extends android.app.Activity { public void *(android.view.View); }
-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}


##---------------Begin: proguard configuration for rxjava  ----------
-keep class io.reactivex.android.schedulers.AndroidSchedulers { public static <methods>; }
-keep class io.reactivex.Schedulers { public static <methods>; }
-keep class io.reactivex.ImmediateScheduler { public <methods>; }
-keep class io.reactivex.TestScheduler { public <methods>; }
-keep class io.reactivex.Schedulers {  public static ** test(); }
-keepclassmembers class rx.internal.util.unsafe.*ArrayQueue*Field* {
   long producerIndex;
   long consumerIndex;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueProducerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode producerNode;
}

-keepclassmembers class rx.internal.util.unsafe.BaseLinkedQueueConsumerNodeRef {
    rx.internal.util.atomic.LinkedQueueNode consumerNode;
}

##---------------Begin ----------
-dontwarn javax.lang.**
-dontwarn javax.tools.**
-dontwarn javax.annotation.**
-dontwarn javax.lang.**

##---------------Begin: proguard configuration for Dagger  ----------
-keepattributes *Annotation*
-keepattributes *Provides*
-keepattributes *Singleton*
-keepattributes *Inject*
-keepattributes *Module*
-keepattributes *Named*
-keep @interface dagger.*
-keep @dagger.Module class *
-keepclassmembers class * { @javax.inject.* <fields>; }
-keepclasseswithmembernames class * { @javax.inject.* <fields>; }
-keep class **$$ModuleAdapter
-keep class **$$InjectAdapter
-keep class **$$StaticInjection
-keep class dagger.** { *; }
-dontwarn dagger.**
-dontwarn com.google.errorprone.annotations.**
-keep class com.google.errorprone.annotations.** { *; }

