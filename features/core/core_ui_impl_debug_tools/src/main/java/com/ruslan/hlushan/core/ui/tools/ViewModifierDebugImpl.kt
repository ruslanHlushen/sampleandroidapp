package com.ruslan.hlushan.core.ui.tools

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import androidx.drawerlayout.widget.DrawerLayout
import com.ruslan.hlushan.ui.api.utils.ViewModifier
import com.ruslan.hlushan.core.ui.impl.tools.R

internal class ViewModifierDebugImpl : ViewModifier {

    override fun modify(view: View): View {
        val drawerLayout = DrawerLayout(view.context)

        drawerLayout.addView(view, DrawerLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT))

        val developerView = LayoutInflater.from(view.context).inflate(R.layout.developer_settings_view, drawerLayout, false)
        val layoutParams = DrawerLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT)
        layoutParams.gravity = Gravity.END
        drawerLayout.addView(developerView, layoutParams)

        return drawerLayout
    }
}