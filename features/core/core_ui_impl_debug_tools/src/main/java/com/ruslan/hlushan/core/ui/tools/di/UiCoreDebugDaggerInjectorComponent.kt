package com.ruslan.hlushan.core.ui.tools.di

import android.app.Application
import com.ruslan.hlushan.core.api.di.ChuckToolProvider
import com.ruslan.hlushan.core.api.di.InjectorHolder
import com.ruslan.hlushan.core.api.di.LeakCanaryToolProvider
import com.ruslan.hlushan.core.api.di.LoggersProvider
import com.ruslan.hlushan.core.api.di.SchedulersProvider
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import com.ruslan.hlushan.core.ui.tools.DeveloperSettingsFragment
import com.ruslan.hlushan.core.ui.tools.ViewModifierDebugImpl
import com.ruslan.hlushan.ui.api.utils.ViewModifier
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import dagger.Provides

@Component(dependencies = [
    LoggersProvider::class,
    SchedulersProvider::class,
    LeakCanaryToolProvider::class,
    ChuckToolProvider::class
])
internal interface UiCoreDebugComponent {

    fun inject(fragment: DeveloperSettingsFragment)

    @Component.Factory
    interface Factory {
        @SuppressWarnings("LongParameterList")
        fun create(
                @BindsInstance initAppConfig: InitAppConfig,
                loggersProvider: LoggersProvider,
                schedulersProvider: SchedulersProvider,
                leakCanaryToolProvider: LeakCanaryToolProvider,
                chuckToolProvider: ChuckToolProvider
        ): UiCoreDebugComponent
    }
}

@SuppressWarnings("UnsafeCast")
internal fun Application.getUiCoreDebugComponent(): UiCoreDebugComponent {
    val injectorHolder = (this as InjectorHolder)
    val components = injectorHolder.components
    return components.getOrPut(UiCoreDebugComponent::class) {
        DaggerUiCoreDebugComponent.factory()
                .create(
                        initAppConfig = (injectorHolder.initAppConfig),
                        loggersProvider = (injectorHolder.iBaseInjector as LoggersProvider),
                        schedulersProvider = (injectorHolder.iBaseInjector as SchedulersProvider),
                        leakCanaryToolProvider = (injectorHolder.iBaseInjector as LeakCanaryToolProvider),
                        chuckToolProvider = (injectorHolder.iBaseInjector as ChuckToolProvider)
                )
    }
}

@Module
object DebugUiToolsModule {

    @JvmStatic
    @Provides
    fun provideViewModifier(): ViewModifier = ViewModifierDebugImpl()
}