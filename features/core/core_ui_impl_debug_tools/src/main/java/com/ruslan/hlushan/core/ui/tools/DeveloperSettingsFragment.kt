package com.ruslan.hlushan.core.ui.tools

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Switch
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.ruslan.hlushan.core.api.tools.ChuckTool
import com.ruslan.hlushan.core.api.tools.LeakCanaryTool
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import com.ruslan.hlushan.core.ui.impl.tools.R
import com.ruslan.hlushan.core.ui.tools.di.getUiCoreDebugComponent
import com.ruslan.hlushan.extensions.setThrottledOnClickListener
import javax.inject.Inject

internal class DeveloperSettingsFragment : Fragment(R.layout.fragment_developer_settings) {

    @Inject lateinit var initAppConfig: InitAppConfig

    @Inject lateinit var leakCanaryTool: LeakCanaryTool
    @Inject lateinit var chuckTool: ChuckTool

    private var textViewVersionCode: TextView? = null
    private var textViewVersionName: TextView? = null

    private var switchLeakCanary: Switch? = null

    private var btnOpenChuck: View? = null
    private var btnOpenLeakCanary: View? = null

    override fun onAttach(context: Context) {
        injectDagger2()
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        setUpViewListeners()
    }

    override fun onResume() {
        super.onResume()

        textViewVersionCode?.text = initAppConfig.versionCode.toString()
        textViewVersionName?.text = initAppConfig.versionName

        switchLeakCanary?.isChecked = leakCanaryTool.enabled
    }

    override fun onDestroyView() {
        clearViews()
        super.onDestroyView()
    }

    private fun injectDagger2() = activity?.application?.getUiCoreDebugComponent()?.inject(this)

    private fun initViews() {
        textViewVersionCode = view?.findViewById(R.id.fragment_dev_settings_version_code_text_view)
        textViewVersionName = view?.findViewById(R.id.fragment_dev_settings_version_name_text_view)

        switchLeakCanary = view?.findViewById(R.id.fragment_dev_settings_leak_canary_switch)

        btnOpenChuck = view?.findViewById(R.id.fragment_dev_settings_open_chuck_btn)
        btnOpenLeakCanary = view?.findViewById(R.id.fragment_dev_settings_open_leak_canary_btn)
    }

    private fun setUpViewListeners() {
        switchLeakCanary?.setOnCheckedChangeListener { buttonView, isChecked -> leakCanaryTool.enabled = isChecked }

        btnOpenChuck?.setThrottledOnClickListener { chuckTool.openScreen() }
        btnOpenLeakCanary?.setThrottledOnClickListener { leakCanaryTool.openScreen() }
    }

    private fun clearViews() {
        textViewVersionCode = null
        textViewVersionName = null

        switchLeakCanary = null

        btnOpenChuck = null
        btnOpenLeakCanary = null
    }
}