package com.ruslan.hlushan.core.impl.tools.di

import android.content.Context
import com.ruslan.hlushan.core.api.tools.ChuckTool
import com.ruslan.hlushan.core.api.tools.LeakCanaryTool
import com.ruslan.hlushan.core.impl.tools.impl.ChuckToolDebugImpl
import com.ruslan.hlushan.core.impl.tools.impl.LeakCanaryToolDebugImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object CoreImplDebugToolsModule {

    @JvmStatic
    @Provides
    fun provideLeakCanaryTool(context: Context): LeakCanaryTool = LeakCanaryToolDebugImpl(context)

    @JvmStatic
    @Provides
    @Singleton
    fun provideChuckTool(context: Context): ChuckTool = ChuckToolDebugImpl(context)
}