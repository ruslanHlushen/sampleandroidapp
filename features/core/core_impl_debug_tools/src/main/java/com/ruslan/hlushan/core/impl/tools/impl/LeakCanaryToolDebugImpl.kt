package com.ruslan.hlushan.core.impl.tools.impl

import android.content.Context
import com.ruslan.hlushan.core.api.tools.LeakCanaryTool
import leakcanary.AppWatcher
import leakcanary.LeakCanary

internal class LeakCanaryToolDebugImpl(private val context: Context) : LeakCanaryTool {

    override var enabled: Boolean
        get() = AppWatcher.config.enabled
        set(newValue) {
            if (enabled != newValue) {
                AppWatcher.config = AppWatcher.config.copy(enabled = newValue)
            }
        }

    override fun openScreen() = context.startActivity(LeakCanary.newLeakDisplayActivityIntent())
}