package com.ruslan.hlushan.core.impl.tools.initUtils

import android.content.Context
import com.facebook.stetho.Stetho

internal fun initStetho(appContext: Context) =
    Stetho.initializeWithDefaults(appContext)