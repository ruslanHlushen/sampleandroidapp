package com.ruslan.hlushan.core.impl.tools.impl

import android.content.Context
import com.chuckerteam.chucker.api.Chucker
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.RetentionManager
import com.ruslan.hlushan.core.api.tools.ChuckTool

internal class ChuckToolDebugImpl(private val context: Context) : ChuckTool {

    private val collector = ChuckerCollector(
            context = context,
            showNotification = true,
            retentionPeriod = RetentionManager.Period.ONE_HOUR)

    override fun logError(tag: String, error: Throwable) = collector.onError(tag, error)

    override fun openScreen() = context.startActivity(Chucker.getLaunchIntent(context, Chucker.SCREEN_HTTP))
}