package com.ruslan.hlushan.core.impl.tools

import android.app.Application
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.impl.tools.initUtils.initSkippedFramesWarning
import com.ruslan.hlushan.core.impl.tools.initUtils.initLeakCanary
import com.ruslan.hlushan.core.impl.tools.initUtils.initStetho
import com.ruslan.hlushan.core.impl.tools.initUtils.initStrictMode

@SuppressWarnings("UnusedPrivateMember")
fun initFirstDebugTools(app: Application, logger: (String) -> Unit) = Unit

fun initSecondDebugTools(app: Application, appLogger: AppLogger, logger: (String) -> Unit) {
    initSkippedFramesWarning(logger)
    logger("after ChoreographerSkippedFramesWarningThreshold()")
    initLeakCanary(app, appLogger)
    logger("after initLeakCanary()")
    initStetho(app)
    logger("after initStetho()")
    initStrictMode()
    logger("after initStrictMode()")
}