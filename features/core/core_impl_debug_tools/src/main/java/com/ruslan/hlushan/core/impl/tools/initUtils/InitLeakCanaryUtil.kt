package com.ruslan.hlushan.core.impl.tools.initUtils

import android.app.Application
import com.ruslan.hlushan.core.api.log.AppLogger
import leakcanary.AppWatcher
import leakcanary.DefaultOnHeapAnalyzedListener
import leakcanary.LeakCanary
import leakcanary.OnHeapAnalyzedListener
import shark.HeapAnalysis
import shark.HeapAnalysisFailure
import shark.HeapAnalysisSuccess
import java.util.concurrent.TimeUnit

internal fun initLeakCanary(application: Application, appLogger: AppLogger) {
//    if (!InstantApps.isInstantApp(application)) {
        @SuppressWarnings("MagicNumber")
        val watchDurationSeconds = 10L
        AppWatcher.config = AppWatcher.config.copy(enabled = true,
                                                   watchActivities = true,
                                                   watchFragments = true,
                                                   watchFragmentViews = true,
                                                   watchDurationMillis = TimeUnit.SECONDS.toMillis(watchDurationSeconds))

        LeakCanary.config = LeakCanary.config.copy(requestWriteExternalStoragePermission = true,
                                                   onHeapAnalyzedListener = AppAnalysisResultListener(application, appLogger))
//    }
}

//TODO: CHECK is working?????
//TODO: uploading to server example: https://square.github.io/leakcanary/recipes/#uploading-to-bugsnag
private class AppAnalysisResultListener(application: Application, private val appLogger: AppLogger) : OnHeapAnalyzedListener {

    private val defaultOnHeapAnalyzedListener = DefaultOnHeapAnalyzedListener(application)

    override fun onHeapAnalyzed(heapAnalysis: HeapAnalysis) {

        when (heapAnalysis) {
            is HeapAnalysisFailure -> {
                appLogger.logClass(AppAnalysisResultListener::class.java, heapAnalysis.toString(), heapAnalysis.exception)
            }
            is HeapAnalysisSuccess -> {
                appLogger.logClass(AppAnalysisResultListener::class.java, heapAnalysis.toString())
            }
        }

        defaultOnHeapAnalyzedListener.onHeapAnalyzed(heapAnalysis)
    }
}