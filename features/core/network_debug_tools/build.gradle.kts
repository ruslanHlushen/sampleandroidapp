plugins {
    id("com.android.library")
}

apply(from = extra["gradle_support_base_android"])
apply(from = extra["gradle_support_build_variants"])

val networkDebugToolsLibraryVersionName = "1"
val networkDebugToolsLibraryVersionCode = 1

android {
    defaultConfig {
        versionCode = networkDebugToolsLibraryVersionCode
        versionName = networkDebugToolsLibraryVersionName
    }

    resourcePrefix("network_debug_tools_")
}

dependencies {
    implementation(project(":core_api"))

    compileOnly(Deps.okHttp3)

    implementation(Deps.okHttp3Logging)

    compileOnly(Deps.chucker)
}