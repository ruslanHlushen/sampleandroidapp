package com.ruslan.hlushan.network.tools

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.chuckerteam.chucker.api.RetentionManager
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.utils.NetworkConfig
import okhttp3.Interceptor
import okhttp3.logging.HttpLoggingInterceptor

fun provideDebugInterceptors(networkConfig: NetworkConfig, appLogger: AppLogger, context: Context): List<Interceptor> =
        if (networkConfig.isNetworkLogEnabled) {
            listOf(provideHttpLoggingInterceptor(appLogger), provideChuckInterceptor(context))
        } else {
            emptyList()
        }

private fun provideChuckInterceptor(context: Context): ChuckerInterceptor {
    val collector = ChuckerCollector(context = context,
                                     showNotification = true,
                                     retentionPeriod = RetentionManager.Period.ONE_HOUR)
    return ChuckerInterceptor(context = context,
                              collector = collector,
                              maxContentLength = 120_000L)
}

private fun provideHttpLoggingInterceptor(appLogger: AppLogger): HttpLoggingInterceptor {
    val logging = HttpLoggingInterceptor { message ->
        appLogger.log("NETWORK-API-HTTP-CALL", message)
    }

    logging.level = HttpLoggingInterceptor.Level.BODY

    return logging
}