package com.ruslan.hlushan.core.impl.tools

import android.util.Log
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.impl.BaseApplication
import com.ruslan.hlushan.core.impl.tools.di.CoreImplDebugToolsModule
import dagger.Module

internal fun BaseApplication.initDebugToolsFirst(appInitializationTag: String) =
        initFirstDebugTools(this) { message ->
            Log.i(APP_TAG, "$appInitializationTag -> $message")
        }

internal fun BaseApplication.initDebugToolsSecond(appInitializationTag: String, appLogger: AppLogger) =
        initSecondDebugTools(this, appLogger) { message ->
            Log.i(APP_TAG, "$appInitializationTag -> $message")
        }

@Module(includes = [CoreImplDebugToolsModule::class])
internal object CoreImplToolsModule