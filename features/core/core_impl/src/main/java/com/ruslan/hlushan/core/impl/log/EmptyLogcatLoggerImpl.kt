package com.ruslan.hlushan.core.impl.log

import com.ruslan.hlushan.core.api.log.LogcatLogger

class EmptyLogcatLoggerImpl : LogcatLogger {

    override fun log(tag: String, logMessage: String) = Unit

    override fun log(tag: String, logMessage: String?, error: Throwable?) = Unit
}