@file:SuppressWarnings("MatchingDeclarationName")

package com.ruslan.hlushan.core.impl.utils

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.annotation.DrawableRes
import androidx.annotation.StringDef
import androidx.core.content.ContextCompat
import com.ruslan.hlushan.core.api.utils.currentLocale
import com.ruslan.hlushan.core.api.utils.wrapContextWithNewLanguage

private const val STRING_DEF_TYPE: String = "string"
private const val DRAWABLE_DEF_TYPE: String = "drawable"

@SuppressWarnings("UnusedPrivateClass")
@StringDef(STRING_DEF_TYPE, DRAWABLE_DEF_TYPE)
private annotation class ResDefType

internal fun getWrappedOrUpdateContext(context: Context, currentAppLangNotFullCode: String): Context? {
    val currentResourcesLang = context.resources.configuration.currentLocale.language
    return if (currentResourcesLang == currentAppLangNotFullCode) {
        context
    } else {
        context.wrapContextWithNewLanguage(currentAppLangNotFullCode)
    }
}

internal fun Context.getStringResourceByName(stringResName: String): String {
    var value: String? = null
    try {
        val resId = getResIdByName(stringResName, STRING_DEF_TYPE)
        value = resId?.let { getString(it) }
    } catch (ignore: Exception) {
    }

    return if (value != null) value else ""
}

internal fun Context.getDrawableResourceByName(drawableResName: String): Drawable? =
        try {
            val resId = getDrawableResourceIdByName(drawableResName)
            resId?.let { ContextCompat.getDrawable(this, resId) }
        } catch (ignore: Exception) {
            null
        }

@DrawableRes
internal fun Context.getDrawableResourceIdByName(drawableResName: String): Int? =
        try {
            getResIdByName(drawableResName, DRAWABLE_DEF_TYPE)
        } catch (ignore: Exception) {
            null
        }

private fun Context.getResIdByName(resName: String, @ResDefType defType: String): Int? =
        try {
            val packageName = packageName
            resources.getIdentifier(resName, defType, packageName)
        } catch (ignore: Exception) {
            null
        }