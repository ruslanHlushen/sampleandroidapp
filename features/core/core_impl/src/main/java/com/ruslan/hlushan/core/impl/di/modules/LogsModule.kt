package com.ruslan.hlushan.core.impl.di.modules

import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.log.LogcatLogger
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import com.ruslan.hlushan.core.impl.log.AppLoggerImpl
import com.ruslan.hlushan.core.impl.log.EmptyLogcatLoggerImpl
import com.ruslan.hlushan.core.impl.log.LogcatLoggerImpl
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [SupportLogsModule::class])
internal interface LogsModule {

    @Binds
    @Singleton
    fun provideAppLogger(appLogger: AppLoggerImpl): AppLogger
}

@Module
internal object SupportLogsModule {

    @JvmStatic
    @Provides
    @Singleton
    internal fun provideLogcatLogger(initAppConfig: InitAppConfig): LogcatLogger =
            if (initAppConfig.isLogcatEnabled) {
                LogcatLoggerImpl()
            } else {
                EmptyLogcatLoggerImpl()
            }
}