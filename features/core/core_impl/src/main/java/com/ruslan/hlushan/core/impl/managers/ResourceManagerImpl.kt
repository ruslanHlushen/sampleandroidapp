package com.ruslan.hlushan.core.impl.managers

import android.content.Context
import android.content.res.Resources
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.impl.utils.getStringResourceByName
import com.ruslan.hlushan.core.impl.utils.getDrawableResourceIdByName
import com.ruslan.hlushan.core.impl.utils.getWrappedOrUpdateContext
import javax.inject.Inject

internal class ResourceManagerImpl
@Inject
constructor(
    private var appContext: Context
) : ResourceManager {

    private val wrappedResources: Resources
        get() {
            wrapContext()
            return appContext.resources
        }

    @SuppressWarnings("TooGenericExceptionCaught")
    override fun getString(@StringRes strResId: Int): String =
        try {
            val srt = wrappedResources.getString(strResId)
            if (!srt.isNullOrEmpty()) srt else ""
        } catch (e: Exception) {
            ""
        }

    @SuppressWarnings("TooGenericExceptionCaught", "SpreadOperator")
    override fun getString(@StringRes strResId: Int, vararg formatArgs: Any): String =
        try {
            val srt = wrappedResources.getString(strResId, *formatArgs)
            if (!srt.isNullOrEmpty()) srt else ""
        } catch (e: Exception) {
            ""
        }

    @SuppressWarnings("TooGenericExceptionCaught")
    override fun getNonTranslatableString(@StringRes strResId: Int): String =
        try {
            val srt = appContext.getString(strResId)
            if (!srt.isNullOrEmpty()) srt else ""
        } catch (e: Exception) {
            ""
        }

    override fun getStringResourceByName(stringResName: String): String {
        wrapContext()
        return appContext.getStringResourceByName(stringResName)
    }

    @DrawableRes
    override fun getDrawableResourceIdByName(drawableResName: String): Int? =
        appContext.getDrawableResourceIdByName(drawableResName)

    private fun wrapContext() {
        val nonNullContext = appContext
        appContext = getWrappedOrUpdateContext(appContext, currentAppLangNotFullCode = "en")
            ?: nonNullContext
    }
}