package com.ruslan.hlushan.core.impl

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatDelegate
import com.ruslan.hlushan.core.api.di.IBaseInjector
import com.ruslan.hlushan.core.api.di.InjectorHolder
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.log.ErrorLogger
import com.ruslan.hlushan.core.impl.tools.initDebugToolsFirst
import com.ruslan.hlushan.core.impl.tools.initDebugToolsSecond
import com.ruslan.hlushan.core.impl.utils.exceptions.RxErrorHandlingUtil
import javax.inject.Inject

private const val APP_INITIALIZATION_TAG: String = "APP_INITIALIZATION"

abstract class BaseApplication : Application(), InjectorHolder {

    @Inject protected lateinit var errorLogger: ErrorLogger
    @Inject protected lateinit var appLogger: AppLogger

    @get:SuppressWarnings("VariableNaming")
    abstract val APP_TAG: String
    abstract override val iBaseInjector: IBaseInjector

    override fun attachBaseContext(base: Context) {
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> before super.attachBaseContext(Context)")
        super.attachBaseContext(base)
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> after super.attachBaseContext(Context) and before MultiDex.install(Context)")
    }

    override fun onCreate() {
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> before super.onCreate()")
        super.onCreate()
        initCrashlytics()
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> after super.onCreate()")
        initFirst()
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> before initDagger2AndInject()")
        initDagger2AndInject()
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> after initDagger2AndInject()")
        initSecond()
    }

    protected abstract fun initDagger2AndInject()

    protected abstract fun initCrashlytics()

    private fun initFirst() {
        this.initDebugToolsFirst(APP_INITIALIZATION_TAG)
    }

    private fun initSecond() {
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> initSecond()")
        setAppErrorHandling()
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> after setAppErrorHandling()")
        initCrashlytics()
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> after initCrashlytics()")
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)
        Log.i(APP_TAG, "$APP_INITIALIZATION_TAG -> after AppCompatDelegate.setCompatVectorFromResourcesEnabled(true)")
        this.initDebugToolsSecond(APP_INITIALIZATION_TAG, appLogger)
    }

    private fun setAppErrorHandling() {
        RxErrorHandlingUtil.setRxErrorHandling(appLogger)
    }
}