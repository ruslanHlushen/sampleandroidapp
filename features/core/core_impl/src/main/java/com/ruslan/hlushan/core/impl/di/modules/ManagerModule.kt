package com.ruslan.hlushan.core.impl.di.modules

import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.impl.managers.ResourceManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
internal interface ManagerModule {

    @Binds
    @Singleton
    fun resourceManager(resourceManagerImpl: ResourceManagerImpl): ResourceManager
}