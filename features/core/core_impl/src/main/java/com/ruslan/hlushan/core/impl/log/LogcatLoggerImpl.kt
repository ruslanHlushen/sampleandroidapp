package com.ruslan.hlushan.core.impl.log

import android.util.Log
import com.ruslan.hlushan.core.api.log.LogcatLogger

internal class LogcatLoggerImpl : LogcatLogger {

    override fun log(tag: String, logMessage: String) {
        Log.i(tag, logMessage)
    }

    override fun log(tag: String, logMessage: String?, error: Throwable?) {
        Log.e(tag, logMessage, error)
    }
}