package com.ruslan.hlushan.core.impl.di.modules

import android.content.Context
import com.ruslan.hlushan.core.impl.BaseApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal object ApplicationModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideContext(app: BaseApplication): Context = app.applicationContext
}