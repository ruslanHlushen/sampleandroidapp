package com.ruslan.hlushan.core.impl.di

import com.ruslan.hlushan.core.api.di.CoreProvider
import com.ruslan.hlushan.core.api.log.ErrorLogger
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import com.ruslan.hlushan.core.impl.BaseApplication
import com.ruslan.hlushan.core.impl.di.modules.ApplicationModule
import com.ruslan.hlushan.core.impl.di.modules.LogsModule
import com.ruslan.hlushan.core.impl.di.modules.ManagerModule
import com.ruslan.hlushan.core.impl.di.modules.SchedulersModule
import com.ruslan.hlushan.core.impl.tools.CoreImplToolsModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Component(modules = [
    ApplicationModule::class,
    LogsModule::class,
    ManagerModule::class,
    SchedulersModule::class,
    CoreImplToolsModule::class
])
@Singleton
interface CoreImplExportComponent : CoreProvider {

    @Component.Factory
    interface Factory {
        fun create(
                @BindsInstance baseApplication: BaseApplication,
                @BindsInstance initAppConfig: InitAppConfig,
                @BindsInstance errorLogger: ErrorLogger
        ): CoreImplExportComponent
    }

    object Initializer {

        fun init(
                baseApplication: BaseApplication,
                initAppConfig: InitAppConfig,
                errorLogger: ErrorLogger
        ): CoreProvider =
                DaggerCoreImplExportComponent.factory()
                        .create(
                                baseApplication = baseApplication,
                                initAppConfig = initAppConfig,
                                errorLogger = errorLogger
                        )
    }
}