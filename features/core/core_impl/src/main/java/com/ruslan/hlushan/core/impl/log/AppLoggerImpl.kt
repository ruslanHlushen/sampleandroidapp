package com.ruslan.hlushan.core.impl.log

import android.util.Log
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.log.ErrorLogger
import com.ruslan.hlushan.core.api.log.LogcatLogger
import com.ruslan.hlushan.core.api.tools.ChuckTool
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import javax.inject.Inject

private fun String?.toDefaultMessageIfEmpty(): String =
    if (this.isNullOrBlank()) {
        "empty_message"
    } else {
        this
    }

private fun String?.createMessage(): String = " :-${toDefaultMessageIfEmpty()}"

private fun Throwable?.createMessage(): String {
    return this?.let {
        "${it::class.java.name}(${it.message.toDefaultMessageIfEmpty()}) \n ${Log.getStackTraceString(
            it
        )}"
    } ?: "unknown_throwable"
}

private fun String?.createMessage(throwable: Throwable?): String =
    " :-${toDefaultMessageIfEmpty()} : ${throwable.createMessage()}"

internal class AppLoggerImpl
@Inject
constructor(
    private val logcatLogger: LogcatLogger,
    private val errorLogger: ErrorLogger,
    private val chuckTool: ChuckTool,
    private val initAppConfig: InitAppConfig
) : AppLogger {

    @Suppress("MagicNumber")
    private val methodName: String
        get() = ".${Thread.currentThread().stackTrace[4].methodName}()"

    override fun log(any: Any) = logMessageIfMakeSense {
        val logMessage = "${any.javaClass.simpleName}$methodName"
        logcatLogger.log(initAppConfig.appTag, logMessage)
    }

    override fun log(any: Any, message: String?) = logMessageIfMakeSense {
        val logMessage = "${any.javaClass.simpleName}$methodName${message.createMessage()}"
        logcatLogger.log(initAppConfig.appTag, logMessage)
    }

    override fun log(any: Any, message: String?, error: Throwable) {
        logMessageIfMakeSense {
            val logMessage = "${any.javaClass.simpleName}$methodName${message.createMessage(error)}"
            logcatLogger.log(initAppConfig.appTag, logMessage, error)
        }
        errorLogger.logError(error)
        chuckTool.logError(message.toDefaultMessageIfEmpty(), error)
    }

    override fun logClass(clazz: Class<*>) = logMessageIfMakeSense {
        val logMessage = "${clazz.simpleName}$methodName"
        logcatLogger.log(initAppConfig.appTag, logMessage)
    }

    override fun logClass(clazz: Class<*>, message: String?) = logMessageIfMakeSense {
        val logMessage = "${clazz.simpleName}$methodName${message.createMessage()}"
        logcatLogger.log(initAppConfig.appTag, logMessage)
    }

    override fun logClass(clazz: Class<*>, message: String?, error: Throwable) {
        logMessageIfMakeSense {
            val logMessage = "${clazz.simpleName}$methodName${message.createMessage(error)}"
            logcatLogger.log(initAppConfig.appTag, logMessage, error)
        }
        errorLogger.logError(error)
        chuckTool.logError(message.toDefaultMessageIfEmpty(), error)
    }

    private inline fun logMessageIfMakeSense(log: () -> Unit) {
        if (initAppConfig.isLogcatEnabled || initAppConfig.isFileLogEnabled) {
            log()
        }
    }
}