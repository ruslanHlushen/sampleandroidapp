package com.ruslan.hlushan.core.impl.di.modules

import com.ruslan.hlushan.core.api.managers.SchedulersManager
import com.ruslan.hlushan.core.impl.managers.SchedulersManagerImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
internal interface SchedulersModule {

    @Binds
    @Singleton
    fun provideSchedulersManager(schedulersManagerImpl: SchedulersManagerImpl): SchedulersManager
}