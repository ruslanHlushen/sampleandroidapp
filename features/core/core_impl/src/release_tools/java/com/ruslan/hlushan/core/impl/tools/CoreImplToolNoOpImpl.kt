package com.ruslan.hlushan.core.impl.tools

import com.ruslan.hlushan.core.api.tools.AndroidDevMetricsTool
import com.ruslan.hlushan.core.api.tools.BlockCanaryTool
import com.ruslan.hlushan.core.api.tools.LeakCanaryTool
import com.ruslan.hlushan.core.api.tools.LynxTool
import com.ruslan.hlushan.core.api.tools.TinyDancerTool
import com.ruslan.hlushan.core.api.tools.TaktTool
import com.ruslan.hlushan.core.api.tools.ChuckTool
import com.ruslan.hlushan.core.api.tools.DatabaseViewerTool

internal class LeakCanaryToolNoOpImpl : LeakCanaryTool {

    override var enabled: Boolean
        get() = false
        set(value) {}

    override fun openScreen() = Unit
}

internal class ChuckToolNoOpImpl : ChuckTool {

    override fun logError(tag: String, error: Throwable) = Unit
    override fun openScreen() = Unit
}