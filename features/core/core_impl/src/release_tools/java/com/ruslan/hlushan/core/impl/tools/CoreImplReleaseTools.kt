package com.ruslan.hlushan.core.impl.tools

import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.tools.BlockCanaryTool
import com.ruslan.hlushan.core.api.tools.LeakCanaryTool
import com.ruslan.hlushan.core.api.tools.LynxTool
import com.ruslan.hlushan.core.api.tools.TinyDancerTool
import com.ruslan.hlushan.core.api.tools.TaktTool
import com.ruslan.hlushan.core.api.tools.ChuckTool
import com.ruslan.hlushan.core.api.tools.AndroidDevMetricsTool
import com.ruslan.hlushan.core.api.tools.DatabaseViewerTool
import com.ruslan.hlushan.core.impl.BaseApplication
import dagger.Module
import dagger.Provides

internal fun BaseApplication.initDebugToolsFirst(appInitializationTag: String) = Unit

internal fun BaseApplication.initDebugToolsSecond(appInitializationTag: String, appLogger: AppLogger) = Unit

@Module
internal object CoreImplToolsModule {

    @JvmStatic
    @Provides
    fun provideLeakCanaryTool(): LeakCanaryTool = LeakCanaryToolNoOpImpl()

    @JvmStatic
    @Provides
    fun provideChuckTool(): ChuckTool = ChuckToolNoOpImpl()
}