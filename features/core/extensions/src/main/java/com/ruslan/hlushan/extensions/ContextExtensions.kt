package com.ruslan.hlushan.extensions

import android.content.Context
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import android.util.TypedValue
import androidx.annotation.AttrRes
import androidx.annotation.ColorRes

@ColorInt
fun Context.colorAttributeValue(@AttrRes colorAttrResId: Int): Int {
    val typedValue = TypedValue()

    val a = obtainStyledAttributes(typedValue.data, intArrayOf(colorAttrResId))
    @ColorInt val color = a.getColor(0, 0)

    a.recycle()

    return color
}

@ColorInt
fun Context.getContextCompatColor(@ColorRes colorResId: Int) = ContextCompat.getColor(this, colorResId)