package com.ruslan.hlushan.extensions

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager

val RecyclerView.lastVisibleItemPosition: Int
    get() {
        val manager = layoutManager ?: return 0
        return when (manager) {
            is LinearLayoutManager        -> manager.findLastVisibleItemPosition()
            is StaggeredGridLayoutManager -> manager.findLastVisibleItemPositions(null).max() ?: 0
            else                          -> 0
        }
    }

private class PaginationScrollListener(private val onPaginationScrollListener: OnPaginationScrollListener) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        if (dy > 0) {
            recyclerView.provideScrollDataToCallback(onPaginationScrollListener)
        }
    }
}

typealias OnPaginationScrollListener = (lastVisibleItem: Int) -> Unit

fun RecyclerView.addPaginationScrollListener(onPaginationScrollListener: OnPaginationScrollListener) {
    this.addOnScrollListener(PaginationScrollListener(onPaginationScrollListener))
}

fun RecyclerView.provideScrollDataToCallback(onPaginationScrollListener: OnPaginationScrollListener) {
    onPaginationScrollListener(this.lastVisibleItemPosition)
}