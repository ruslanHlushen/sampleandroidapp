package com.ruslan.hlushan.extensions

import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide

fun ImageView.loadUrl(
    url: String?,
    target: Fragment,
    @DrawableRes placeholderResId: Int? = null,
    @DrawableRes errorResId: Int? = null
) {
    var requestCreator = Glide.with(target).load(url)

    if (placeholderResId != null) {
        requestCreator = requestCreator.placeholder(placeholderResId)
    }

    if (errorResId != null) {
        requestCreator = requestCreator.error(errorResId)
    }

    requestCreator.into(this)
}