package com.ruslan.hlushan.extensions

val <T> T.exhaustive: T get() = this