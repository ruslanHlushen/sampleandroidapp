package com.ruslan.hlushan.extensions

import android.view.View

fun View.setThrottledOnClickListener(
    intervalMillis: Long = ThrottledOnClickListener.DEFAULT_INTERVAL_MILLIS,
    onClickListener: ((View) -> Unit)?
) =
    setOnClickListener(if (onClickListener != null) {
        ThrottledOnClickListener(intervalMillis, onClickListener)
    } else {
        null
    })