package com.ruslan.hlushan.extensions

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

fun Disposable.safetyDispose() {
    if (!isDisposed) {
        dispose()
    }
}

val Disposable?.isActive: Boolean
    get() = if (this != null) {
        !this.isDisposed
    } else {
        false
    }

operator fun CompositeDisposable.plusAssign(disposable: Disposable) {
    add(disposable)
}