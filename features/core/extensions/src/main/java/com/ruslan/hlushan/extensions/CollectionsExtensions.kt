package com.ruslan.hlushan.extensions

inline fun <T> MutableCollection<T>.clearAndAddAll(newValues: Iterable<T>): Boolean {
    clear()
    return addAll(newValues)
}

inline fun <T : Any?> List<T>.indexOfOrNull(item: T): Int? =
    this.indexOf(item).takeIf { index -> index in this.indices }