plugins {
    id("com.android.library")
}

apply(from = extra["gradle_support_base_android"])
apply(from = extra["gradle_support_build_variants"])

val extensionsLibraryVersionName = "1"
val extensionsLibraryVersionCode = 1

val glideProguardRulesFile = "proguard-rules-glide.pro"

android {
    defaultConfig {
        versionCode = extensionsLibraryVersionCode
        versionName = extensionsLibraryVersionName

        consumerProguardFiles(glideProguardRulesFile)
    }

    buildTypes {
        getByName(BuildTypes.release) {
            isMinifyEnabled = false
        }
    }

    resourcePrefix("extensions_")
}

dependencies {
    compileOnly(Deps.rxJava2)

    api(Deps.ktx)
    implementation(Deps.supportAppCompat)
    implementation(Deps.supportDesign)

    implementation(Deps.glide)
}