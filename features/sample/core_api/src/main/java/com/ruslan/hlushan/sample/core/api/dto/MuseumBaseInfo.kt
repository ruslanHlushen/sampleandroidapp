package com.ruslan.hlushan.sample.core.api.dto

data class MuseumBaseInfo(
    val id: String,
    val objectNumber: String,
    val title: String,
    val webImage: Image?,
    val headerImage: Image?
)