package com.ruslan.hlushan.sample.core.api.dto

class KeysConfig(
    val apiUrl: String,
    val apiKey: String
)