package com.ruslan.hlushan.sample.core.api.dto

data class Image(
    val url: String
)