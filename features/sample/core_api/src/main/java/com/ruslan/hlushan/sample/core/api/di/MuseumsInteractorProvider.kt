package com.ruslan.hlushan.sample.core.api.di

import com.ruslan.hlushan.sample.core.api.MuseumsInteractor

interface MuseumsInteractorProvider {

    fun provideMuseumsInteractor(): MuseumsInteractor
}