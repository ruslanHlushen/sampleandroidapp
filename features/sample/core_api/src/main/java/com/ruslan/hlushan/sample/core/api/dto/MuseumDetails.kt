package com.ruslan.hlushan.sample.core.api.dto

data class MuseumDetails(
    val id: String,
    val objectNumber: String,
    val title: String,
    val longTitle: String,
    val subTitle: String,
    val description: String,
    val webImage: Image?,
    val titles: List<String>
)