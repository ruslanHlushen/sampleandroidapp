package com.ruslan.hlushan.sample.core.api

import com.ruslan.hlushan.core.api.dto.PaginationResponse
import com.ruslan.hlushan.sample.core.api.dto.MuseumBaseInfo
import com.ruslan.hlushan.sample.core.api.dto.MuseumDetails
import io.reactivex.Single

interface MuseumsInteractor {

    fun loadPage(
        nextId: Int?,
        pageSize: Int,
        search: String
    ): Single<PaginationResponse<MuseumBaseInfo, Int>>

    fun getDetails(objectNumber: String): Single<MuseumDetails>
}