plugins {
    id("com.android.library")
}

apply(from = extra["gradle_support_base_android"])
apply(from = extra["gradle_support_build_variants"])

val sampleCoreApiLibraryVersionName = "1"
val sampleCoreApiLibraryVersionCode = 1

android {
    defaultConfig {
        versionCode = sampleCoreApiLibraryVersionCode
        versionName = sampleCoreApiLibraryVersionName
    }

    buildTypes {
        getByName(BuildTypes.release) {
            isMinifyEnabled = false
        }
    }

    resourcePrefix("sample_core_api_")
}

dependencies {
    compileOnly(project(":core_api"))
    compileOnly(project(":extensions"))

    compileOnly(Deps.supportAnnotations)
    compileOnly(Deps.supportAppCompat)

    compileOnly(Deps.rxJava2)
}