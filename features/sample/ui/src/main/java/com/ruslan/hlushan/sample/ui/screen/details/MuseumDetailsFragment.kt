package com.ruslan.hlushan.sample.ui.screen.details

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.ruslan.hlushan.core.api.utils.UiMainThread
import com.ruslan.hlushan.extensions.loadUrl
import com.ruslan.hlushan.sample.core.api.dto.MuseumDetails
import com.ruslan.hlushan.sample.ui.R
import com.ruslan.hlushan.sample.ui.di.getSampleUiComponent
import com.ruslan.hlushan.ui.api.insets.addSystemPadding
import com.ruslan.hlushan.ui.api.presentation.view.fragment.BaseMvpFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.terrakok.cicerone.android.support.SupportAppScreen

private const val KEY_OBJECT_NUMBER = "KEY_OBJECT_NUMBER"

internal class MuseumDetailsFragment : BaseMvpFragment<MuseumDetailsPresenter>(
    layoutResId = R.layout.sample_ui_museum_details_screen
), MuseumDetailsView {

    @InjectPresenter
    lateinit var museumDetailsPresenter: MuseumDetailsPresenter

    private val objectNumberText: TextView? by bindViewById(R.id.sample_ui_museum_details_screen_object_number_text)
    private val titleText: TextView? by bindViewById(R.id.sample_ui_museum_details_screen_title_text)
    private val longTitleText: TextView? by bindViewById(R.id.sample_ui_museum_details_screen_long_title_text)
    private val longTitleGroup: View? by bindViewById(R.id.sample_ui_museum_details_screen_long_title_group)
    private val subTitleText: TextView? by bindViewById(R.id.sample_ui_museum_details_screen_sub_title_text)
    private val subTitleGroup: View? by bindViewById(R.id.sample_ui_museum_details_screen_sub_title_group)
    private val descriptionText: TextView? by bindViewById(R.id.sample_ui_museum_details_screen_description_text)
    private val descriptionGroup: View? by bindViewById(R.id.sample_ui_museum_details_screen_description_group)
    private val titlesText: TextView? by bindViewById(R.id.sample_ui_museum_details_screen_titles_text)
    private val titlesGroup: View? by bindViewById(R.id.sample_ui_museum_details_screen_titles_group)
    private val webImageView: ImageView? by bindViewById(R.id.sample_ui_museum_details_screen_web_image)
    private val screenContentView: View? by bindViewById(R.id.sample_ui_museum_details_screen_content)
    private val progressView: View? by bindViewById(R.id.sample_ui_museum_details_screen_progress)

    @ProvidePresenter
    override fun providePresenter(): MuseumDetailsPresenter =
        getSampleUiComponent().museumDetailsPresenterFactory.create(
            router = parentRouter,
            objectNumber = arguments?.getString(KEY_OBJECT_NUMBER, null).orEmpty()
        )

    override val presenter: MuseumDetailsPresenter get() = museumDetailsPresenter

    override fun injectDagger2() = getSampleUiComponent().inject(this)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.addSystemPadding(top = true, bottom = true)
    }

    @UiMainThread
    override fun showDetails(details: MuseumDetails) {
        objectNumberText?.text = details.objectNumber
        titleText?.text = details.title

        longTitleGroup?.isVisible = details.longTitle.isNotBlank()
        longTitleText?.text = details.longTitle

        subTitleGroup?.isVisible = details.subTitle.isNotBlank()
        subTitleText?.text = details.subTitle

        descriptionGroup?.isVisible = details.description.isNotBlank()
        descriptionText?.text = details.description

        titlesGroup?.isVisible = details.titles.isNotEmpty()
        titlesText?.text = details.titles.joinToString(separator = ";\n")

        descriptionGroup?.isVisible = details.description.isNotBlank()
        descriptionText?.text = details.description

        webImageView?.isVisible = (details.webImage != null)
        webImageView?.loadUrl(
            url = details.webImage?.url,
            target = this,
            placeholderResId = R.drawable.ic_photo_white_24dp,
            errorResId = R.drawable.ic_error_outline_24dp
        )
    }

    @UiMainThread
    override fun showLoading(show: Boolean) {
        progressView?.isVisible = show
        screenContentView?.isInvisible = show
    }
}

internal class MuseumDetailsScreen(private val objectNumber: String) : SupportAppScreen() {
    override fun getFragment(): Fragment = MuseumDetailsFragment().apply {
        val bundle = Bundle(1)
        bundle.putString(KEY_OBJECT_NUMBER, objectNumber)
        arguments = bundle
    }
}