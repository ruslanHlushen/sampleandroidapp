package com.ruslan.hlushan.sample.ui.di

import com.ruslan.hlushan.core.api.di.InjectorHolder
import com.ruslan.hlushan.core.api.di.LoggersProvider
import com.ruslan.hlushan.core.api.di.ManagersProvider
import com.ruslan.hlushan.core.api.di.SchedulersProvider
import com.ruslan.hlushan.sample.core.api.di.MuseumsInteractorProvider
import com.ruslan.hlushan.sample.ui.screen.details.MuseumDetailsFragment
import com.ruslan.hlushan.sample.ui.screen.details.MuseumDetailsPresenter
import com.ruslan.hlushan.sample.ui.screen.list.MuseumsListFragment
import com.ruslan.hlushan.sample.ui.screen.list.MuseumsListPresenter
import com.ruslan.hlushan.ui.api.di.UiCoreProvider
import com.ruslan.hlushan.ui.api.presentation.view.fragment.BaseFragment
import com.squareup.inject.assisted.dagger2.AssistedModule
import dagger.Component
import dagger.Module

@SuppressWarnings("ComplexInterface")
@Component(
    modules = [AssistedSampleUiPresentersModule::class],
    dependencies = [
        UiCoreProvider::class,
        ManagersProvider::class,
        LoggersProvider::class,
        SchedulersProvider::class,
        MuseumsInteractorProvider::class
    ]
)
internal interface SampleUiComponent {

    fun inject(fragment: MuseumsListFragment)
    fun inject(fragment: MuseumDetailsFragment)

    val museumsListPresenterFactory: MuseumsListPresenter.Factory
    val museumDetailsPresenterFactory: MuseumDetailsPresenter.Factory

    @Component.Factory
    interface Factory {
        @SuppressWarnings("LongParameterList")
        fun create(
            uiCoreProvider: UiCoreProvider,
            managersProvider: ManagersProvider,
            loggersProvider: LoggersProvider,
            schedulersProvider: SchedulersProvider,
            museumsInteractorProvider: MuseumsInteractorProvider
        ): SampleUiComponent
    }
}

@AssistedModule
@Module(includes = [AssistedInject_AssistedSampleUiPresentersModule::class])
internal abstract class AssistedSampleUiPresentersModule

@SuppressWarnings("UnsafeCast")
internal fun BaseFragment.getSampleUiComponent(): SampleUiComponent {
    val injectorHolder = (activity?.application as InjectorHolder)
    val components = injectorHolder.components
    return components.getOrPut(SampleUiComponent::class) {
        DaggerSampleUiComponent.factory()
            .create(
                uiCoreProvider = (injectorHolder.iBaseInjector as UiCoreProvider),
                managersProvider = (injectorHolder.iBaseInjector as ManagersProvider),
                loggersProvider = (injectorHolder.iBaseInjector as LoggersProvider),
                schedulersProvider = (injectorHolder.iBaseInjector as SchedulersProvider),
                museumsInteractorProvider = (injectorHolder.iBaseInjector as MuseumsInteractorProvider)
            )
    }
}

internal fun BaseFragment.clearSampleUiComponent() {
    val injectorHolder = (activity?.application as InjectorHolder)
    injectorHolder.components.clear(SampleUiComponent::class)
}