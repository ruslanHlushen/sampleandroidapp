package com.ruslan.hlushan.sample.ui.screen.list

import com.ruslan.hlushan.core.api.utils.UiMainThread
import com.ruslan.hlushan.ui.api.presentation.presenter.PaginationState
import com.ruslan.hlushan.ui.api.presentation.view.IBaseView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.StateStrategyType

internal interface MuseumsListView : IBaseView {

    @UiMainThread
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun setState(items: List<MuseumRecyclerItem.Value>, additional: PaginationState.Additional?)
}