package com.ruslan.hlushan.sample.ui.screen.details

import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.api.managers.SchedulersManager
import com.ruslan.hlushan.sample.core.api.MuseumsInteractor
import com.ruslan.hlushan.ui.api.presentation.presenter.BasePresenter
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import moxy.InjectViewState
import ru.terrakok.cicerone.Router

@InjectViewState
internal class MuseumDetailsPresenter
@AssistedInject
constructor(
    @Assisted objectNumber: String,
    @Assisted router: Router,
    resourceManager: ResourceManager,
    appLogger: AppLogger,
    schedulersManager: SchedulersManager,
    museumsInteractor: MuseumsInteractor
) : BasePresenter<MuseumDetailsView>(
    resourceManager,
    router,
    appLogger,
    schedulersManager
) {

    init {
        museumsInteractor.getDetails(objectNumber = objectNumber)
            .observeOn(schedulersManager.ui)
            .doOnSubscribe { viewState.showLoading(show = true) }
            .doFinally { viewState.showLoading(show = false) }
            .subscribe(
                { details -> viewState.showDetails(details) },
                { error -> viewState.showError(error) }
            ).joinToHard()
    }

    @AssistedInject.Factory
    interface Factory {
        fun create(router: Router, objectNumber: String): MuseumDetailsPresenter
    }
}