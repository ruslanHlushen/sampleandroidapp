package com.ruslan.hlushan.sample.ui.screen.list

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ruslan.hlushan.core.api.utils.UiMainThread
import com.ruslan.hlushan.extensions.ifNotNull
import com.ruslan.hlushan.extensions.loadUrl
import com.ruslan.hlushan.extensions.setThrottledOnClickListener
import com.ruslan.hlushan.sample.core.api.dto.Image
import com.ruslan.hlushan.ui.api.extensions.bindViewById
import com.ruslan.hlushan.sample.core.api.dto.MuseumBaseInfo
import com.ruslan.hlushan.sample.ui.R
import com.ruslan.hlushan.ui.api.adapter.recycler.BaseItemViewHolder
import com.ruslan.hlushan.ui.api.adapter.recycler.BaseRecyclerAdapter
import com.ruslan.hlushan.ui.api.adapter.recycler.OnItemClickListener
import com.ruslan.hlushan.ui.api.adapter.recycler.RecyclerItem

internal class MuseumsRecyclerAdapter(
    private val target: Fragment,
    private val itemClick: OnItemClickListener<MuseumBaseInfo>,
    private val onErrorRetryClickListener: () -> Unit
) : BaseRecyclerAdapter<String, MuseumRecyclerItem>() {

    override fun viewHolder(itemView: View, @LayoutRes type: Int): RecyclerView.ViewHolder =
        when (type) {
            MuseumRecyclerItem.Loader.typeLayoutResId -> LoaderViewHolder(
                itemView
            )
            MuseumRecyclerItem.Error.typeLayoutResId -> ErrorViewHolder(
                itemView,
                onErrorRetryClickListener
            )
            MuseumRecyclerItem.Value.Header.typeLayoutResId -> MuseumHeaderViewHolder(
                itemView,
                target
            )
            else -> MuseumDataViewHolder(
                itemView,
                target,
                itemClick
            )
        }

    @UiMainThread
    override fun setUpWithRecycler(recyclerView: RecyclerView) =
        setUpRecyclerDefaults(
            recyclerView = recyclerView,
            layoutManager = LinearLayoutManager(recyclerView.context),
            hasFixedSize = false
        )
}

private class MuseumDataViewHolder(
    itemView: View,
    private val target: Fragment,
    private val itemClick: OnItemClickListener<MuseumBaseInfo>
) :
    BaseItemViewHolder<String, MuseumRecyclerItem.Value.Data>(itemView) {

    private val title: TextView by bindViewById(R.id.sample_ui_museum_item_title)

    private val webImage: ImageView by bindViewById(R.id.sample_ui_museum_item_error_web_image)

    override fun onViewAttachedToWindow() {
        super.onViewAttachedToWindow()

        itemView.setThrottledOnClickListener { _ ->
            ifNotNull(recyclerItem?.info) { info ->
                itemClick(info)
            }
        }
    }

    override fun onBindView(item: MuseumRecyclerItem.Value.Data) {
        super.onBindView(item)

        webImage.loadUrl(
            url = item.info.webImage?.url,
            target = target,
            errorResId = R.drawable.ic_error_outline_24dp
        )
        title.text = item.info.title
    }

    override fun onViewDetachedFromWindow() {
        itemView.setThrottledOnClickListener(onClickListener = null)
        super.onViewDetachedFromWindow()
    }
}

private class LoaderViewHolder(itemView: View) :
    BaseItemViewHolder<String, MuseumRecyclerItem.Loader>(itemView)

private class MuseumHeaderViewHolder(
    itemView: View,
    private val target: Fragment
) :
    BaseItemViewHolder<String, MuseumRecyclerItem.Value.Header>(itemView) {

    private val headerImage: ImageView by bindViewById(R.id.sample_ui_museum_item_header_image)

    override fun onBindView(item: MuseumRecyclerItem.Value.Header) {
        super.onBindView(item)

        headerImage.loadUrl(
            url = item.image.url,
            target = target,
            errorResId = R.drawable.ic_error_outline_24dp,
            placeholderResId = R.drawable.ic_photo_white_24dp
        )
    }
}

private class ErrorViewHolder(
    itemView: View,
    private val onErrorRetryClickListener: () -> Unit
) : BaseItemViewHolder<String, MuseumRecyclerItem.Error>(itemView) {

    private val errorMessage: TextView by bindViewById(R.id.sample_ui_museum_item_error_message)

    override fun onViewAttachedToWindow() {
        super.onViewAttachedToWindow()

        itemView.setThrottledOnClickListener { _ ->
            onErrorRetryClickListener()
        }
    }

    override fun onBindView(item: MuseumRecyclerItem.Error) {
        super.onBindView(item)

        errorMessage.text = item.userMessage
    }

    override fun onViewDetachedFromWindow() {
        itemView.setThrottledOnClickListener(onClickListener = null)
        super.onViewDetachedFromWindow()
    }
}

internal sealed class MuseumRecyclerItem : RecyclerItem<String> {

    sealed class Value : MuseumRecyclerItem() {

        class Data(val info: MuseumBaseInfo) : Value() {

            override val id: String get() = info.id

            @get:LayoutRes
            override val layoutResId: Int
                get() = R.layout.sample_ui_museum_item

            override fun hasTheSameContent(other: RecyclerItem<String>): Boolean =
                ((other is Data)
                        && (this.info == other.info))
        }

        class Header(val image: Image) : Value() {

            companion object {
                @get:LayoutRes
                val typeLayoutResId
                    get() = R.layout.sample_ui_museum_item_header
            }

            override val id: String get() = image.url

            @get:LayoutRes
            override val layoutResId: Int
                get() = typeLayoutResId

            override fun hasTheSameContent(other: RecyclerItem<String>): Boolean =
                ((other is Header)
                        && (this.image == other.image))
        }
    }

    class Loader : MuseumRecyclerItem() {

        companion object {
            @get:LayoutRes
            val typeLayoutResId
                get() = R.layout.sample_ui_museum_item_loader
        }

        override val id: String get() = "-1"

        @get:LayoutRes
        override val layoutResId: Int
            get() = typeLayoutResId

        override fun hasTheSameContent(other: RecyclerItem<String>): Boolean =
            (other is Loader)
    }

    class Error(val userMessage: String) : MuseumRecyclerItem() {

        companion object {
            @get:LayoutRes
            val typeLayoutResId
                get() = R.layout.sample_ui_museum_item_error
        }

        override val id: String get() = "-2"

        @get:LayoutRes
        override val layoutResId: Int
            get() = typeLayoutResId

        override fun hasTheSameContent(other: RecyclerItem<String>): Boolean =
            (other is Error)
    }
}