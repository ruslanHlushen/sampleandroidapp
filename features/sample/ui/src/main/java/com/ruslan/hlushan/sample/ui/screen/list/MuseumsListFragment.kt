package com.ruslan.hlushan.sample.ui.screen.list

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.textfield.TextInputLayout
import com.ruslan.hlushan.core.api.utils.UiMainThread
import com.ruslan.hlushan.sample.ui.R
import com.ruslan.hlushan.sample.ui.di.getSampleUiComponent
import com.ruslan.hlushan.sample.ui.screen.details.MuseumDetailsScreen
import com.ruslan.hlushan.ui.api.adapter.recycler.BaseRecyclerAdapter
import com.ruslan.hlushan.ui.api.insets.addSystemPadding
import com.ruslan.hlushan.ui.api.presentation.presenter.PaginationState
import com.ruslan.hlushan.ui.api.presentation.view.fragment.BasePaginationFragment
import moxy.presenter.InjectPresenter
import moxy.presenter.ProvidePresenter
import ru.terrakok.cicerone.android.support.SupportAppScreen

internal class MuseumsListFragment : BasePaginationFragment<MuseumsListPresenter>(
    layoutResId = R.layout.sample_ui_museums_list_screen
), MuseumsListView {

    private val museumsAdapter =
        MuseumsRecyclerAdapter(
            target = this,
            itemClick = { info -> parentRouter.navigateTo(MuseumDetailsScreen(objectNumber = info.objectNumber)) },
            onErrorRetryClickListener = { presenter.retry() }
        )

    private val museumsListSearchInput: TextInputLayout? by bindViewById(R.id.sample_ui_museums_list_screen_search_input)
    private val museumsListSwipeRefresh: SwipeRefreshLayout? by bindViewById(R.id.sample_ui_museums_list_screen_swipe_refresh)
    private val museumsListRecycler: RecyclerView? by bindViewById(R.id.sample_ui_museums_list_screen_list)
    private val museumsListEmptyView: View? by bindViewById(R.id.sample_ui_museums_list_screen_empty_view)

    @InjectPresenter
    lateinit var museumsListPresenter: MuseumsListPresenter

    @ProvidePresenter
    override fun providePresenter(): MuseumsListPresenter =
        getSampleUiComponent().museumsListPresenterFactory.create(parentRouter)

    override val presenter: MuseumsListPresenter get() = museumsListPresenter

    override fun injectDagger2() = getSampleUiComponent().inject(this)

    override val recyclerViewItems: RecyclerView? get() = museumsListRecycler
    override val swipeRefreshLayout: SwipeRefreshLayout? get() = museumsListSwipeRefresh

    override val basePaginationRecyclerAdapter: BaseRecyclerAdapter<*, *> get() = museumsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.addSystemPadding(top = true)
        museumsListRecycler?.addSystemPadding(bottom = true)

        museumsListSearchInput?.editText?.doAfterTextChanged { text ->
            presenter.setSearchText(searchText = text.toString())
        }
    }

    @UiMainThread
    override fun setState(
        items: List<MuseumRecyclerItem.Value>,
        additional: PaginationState.Additional?
    ) {

        val isInitLoading = (items.isEmpty() && (additional is PaginationState.Additional.Loading))

        museumsListEmptyView?.isVisible = (additional is PaginationState.Additional.Empty)

        showSwipeProgress(isInitLoading)

        val additionalRecyclerItem: MuseumRecyclerItem? = when {
            isInitLoading -> null
            (additional is PaginationState.Additional.Loading) -> MuseumRecyclerItem.Loader()
            (additional is PaginationState.Additional.Error) -> MuseumRecyclerItem.Error(
                compositeUserErrorMapper.produceUserMessage(additional.value)
            )
            else -> null
        }

        val allItems = if (additionalRecyclerItem != null) {
            (items + additionalRecyclerItem)
        } else {
            items
        }

        museumsAdapter.setAll(allItems)
    }
}

class MuseumsListScreen : SupportAppScreen() {
    override fun getFragment(): Fragment =
        MuseumsListFragment()
}