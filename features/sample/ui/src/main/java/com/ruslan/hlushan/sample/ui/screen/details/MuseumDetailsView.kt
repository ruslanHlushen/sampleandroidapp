package com.ruslan.hlushan.sample.ui.screen.details

import com.ruslan.hlushan.core.api.utils.UiMainThread
import com.ruslan.hlushan.sample.core.api.dto.MuseumDetails
import com.ruslan.hlushan.ui.api.presentation.view.IBaseView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.OneExecutionStateStrategy
import moxy.viewstate.strategy.StateStrategyType

internal interface MuseumDetailsView : IBaseView {

    @UiMainThread
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showLoading(show: Boolean)

    @UiMainThread
    @StateStrategyType(AddToEndSingleStrategy::class)
    fun showDetails(details: MuseumDetails)

    @UiMainThread
    @StateStrategyType(OneExecutionStateStrategy::class)
    fun showError(error: Throwable)
}