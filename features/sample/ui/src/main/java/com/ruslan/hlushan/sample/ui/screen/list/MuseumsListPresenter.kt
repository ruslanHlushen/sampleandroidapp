package com.ruslan.hlushan.sample.ui.screen.list

import com.ruslan.hlushan.core.api.dto.PaginationResponse
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.managers.ResourceManager
import com.ruslan.hlushan.core.api.managers.SchedulersManager
import com.ruslan.hlushan.sample.core.api.MuseumsInteractor
import com.ruslan.hlushan.ui.api.adapter.recycler.RecyclerItem
import com.ruslan.hlushan.ui.api.presentation.presenter.PaginationPresenter
import com.squareup.inject.assisted.Assisted
import com.squareup.inject.assisted.AssistedInject
import io.reactivex.Single
import moxy.InjectViewState
import ru.terrakok.cicerone.Router
import kotlin.random.Random

@InjectViewState
internal class MuseumsListPresenter
@AssistedInject
constructor(
    resourceManager: ResourceManager,
    @Assisted router: Router,
    appLogger: AppLogger,
    schedulersManager: SchedulersManager,
    private val museumsInteractor: MuseumsInteractor
) : PaginationPresenter<String, String, Int, MuseumsListView>(
    initFilter = "",
    resourceManager = resourceManager,
    router = router,
    appLogger = appLogger,
    schedulersManager = schedulersManager
) {

    private val random = Random(System.currentTimeMillis())

    init {
        refresh()
    }

    override fun loadData(
        nextId: Int?,
        filter: String
    ): Single<PaginationResponse<RecyclerItem<String>, Int>> =
        museumsInteractor.loadPage(
            nextId = nextId,
            pageSize = itemsLimitPerPage,
            search = filter
        )
            .map { response ->
                val itemsWithHeaders = response.result.flatMap { info ->
                    val dataItem =
                        MuseumRecyclerItem.Value.Data(
                            info = info
                        )
                    val headerImage = info.headerImage

                    if (random.nextBoolean() && (headerImage != null)) {
                        val header =
                            MuseumRecyclerItem.Value.Header(
                                image = headerImage
                            )
                        listOf(header, dataItem)
                    } else {
                        listOf(dataItem)
                    }
                }

                PaginationResponse.create(
                    result = itemsWithHeaders,
                    nextId = response.nextId
                )
            }

    override fun onStateUpdated() {
        @Suppress("UNCHECKED_CAST")
        viewState.setState(state.items as List<MuseumRecyclerItem.Value>, state.additional)
    }

    fun setSearchText(searchText: String) = updateFilter(searchText.trim())

    @AssistedInject.Factory
    interface Factory {
        fun create(router: Router): MuseumsListPresenter
    }
}