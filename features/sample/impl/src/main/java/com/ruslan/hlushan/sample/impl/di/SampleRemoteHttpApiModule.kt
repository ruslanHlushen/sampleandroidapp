package com.ruslan.hlushan.sample.impl.di

import com.ruslan.hlushan.sample.core.api.dto.KeysConfig
import dagger.Module
import dagger.Provides
import com.ruslan.hlushan.sample.impl.remote.MuseumsRemoteApi
import com.ruslan.hlushan.sample.impl.remote.NetworkApiCreator

@Module
internal object SampleRemoteHttpApiModule {

    @JvmStatic
    @Provides
    fun provideMuseumsRemoteApi(
        networkApiCreator: NetworkApiCreator,
        keysConfig: KeysConfig
    ): MuseumsRemoteApi =
        networkApiCreator.createApi(
            MuseumsRemoteApi::class,
            cacheFolderName = "museums",
            baseUrl = keysConfig.apiUrl
        )
}