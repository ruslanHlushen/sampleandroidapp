package com.ruslan.hlushan.sample.impl.remote.dto

import com.ruslan.hlushan.sample.core.api.dto.MuseumDetails

internal class MuseumDetailsDTO(
    val id: String,
    val objectNumber: String,
    val title: String,
    val longTitle: String?,
    val subTitle: String?,
    val titles: List<String?>?,
    val description: String?,
    val webImage: ImageDTO?
) {
    fun toEntity(): MuseumDetails =
        MuseumDetails(
            id = this.id,
            objectNumber = this.objectNumber,
            title = this.title,
            longTitle = this.longTitle.orEmpty(),
            description = this.description.orEmpty(),
            subTitle = this.subTitle.orEmpty(),
            titles = this.titles?.filterNotNull().orEmpty(),
            webImage = this.webImage?.toEntity()
        )
}