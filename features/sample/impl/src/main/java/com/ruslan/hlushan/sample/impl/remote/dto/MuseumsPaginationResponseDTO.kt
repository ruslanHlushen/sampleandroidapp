package com.ruslan.hlushan.sample.impl.remote.dto

internal class MuseumsPaginationResponseDTO(
    val artObjects: List<MuseumBaseInfoDTO>
)