package com.ruslan.hlushan.sample.impl.remote.dto

import com.ruslan.hlushan.sample.core.api.dto.MuseumBaseInfo

internal class MuseumBaseInfoDTO(
    val id: String,
    val objectNumber: String,
    val title: String,
    val webImage: ImageDTO?,
    val headerImage: ImageDTO?
) {
    fun toEntity(): MuseumBaseInfo =
        MuseumBaseInfo(
            id = this.id,
            objectNumber = this.objectNumber,
            title = this.title,
            webImage = this.webImage?.toEntity(),
            headerImage = this.headerImage?.toEntity()
        )
}