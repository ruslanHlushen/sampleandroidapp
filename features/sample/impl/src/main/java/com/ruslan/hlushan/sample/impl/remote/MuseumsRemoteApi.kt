package com.ruslan.hlushan.sample.impl.remote

import androidx.annotation.IntRange
import com.ruslan.hlushan.sample.impl.remote.dto.MuseumsDetailsResponseDTO
import com.ruslan.hlushan.sample.impl.remote.dto.MuseumsPaginationResponseDTO
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

internal interface MuseumsRemoteApi {

    companion object {
        const val FIRST_PAGE_NUMBER: Int = 0
    }

    @GET("collection")
    fun loadPage(
        @Query("key") apiKey: String,
        @IntRange(from = FIRST_PAGE_NUMBER.toLong()) @Query("p") pageNumber: Int,
        @IntRange(from = 1) @Query("ps") pageSize: Int,
        @Query("q") search: String
    ): Single<MuseumsPaginationResponseDTO>

    @GET("collection/{objectNumber}")
    fun getDetails(
        @Path("objectNumber") objectNumber: String,
        @Query("key") apiKey: String
    ): Single<MuseumsDetailsResponseDTO>
}