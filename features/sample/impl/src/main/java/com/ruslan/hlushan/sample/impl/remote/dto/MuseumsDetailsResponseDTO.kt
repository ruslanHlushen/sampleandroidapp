package com.ruslan.hlushan.sample.impl.remote.dto

internal class MuseumsDetailsResponseDTO(
    val artObject: MuseumDetailsDTO
)