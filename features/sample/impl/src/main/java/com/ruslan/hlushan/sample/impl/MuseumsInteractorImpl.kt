package com.ruslan.hlushan.sample.impl

import androidx.annotation.IntRange
import com.ruslan.hlushan.core.api.dto.PaginationResponse
import com.ruslan.hlushan.sample.core.api.MuseumsInteractor
import com.ruslan.hlushan.sample.core.api.dto.MuseumBaseInfo
import com.ruslan.hlushan.sample.core.api.dto.MuseumDetails
import io.reactivex.Single
import javax.inject.Inject

internal class MuseumsInteractorImpl @Inject constructor(
    private val museumsRepository: MuseumsRepository
) : MuseumsInteractor {

    override fun loadPage(
        nextId: Int?,
        @IntRange(from = 1) pageSize: Int,
        search: String
    ): Single<PaginationResponse<MuseumBaseInfo, Int>> =
        museumsRepository.loadPage(
            nextId = nextId,
            pageSize = pageSize,
            search = search
        )

    override fun getDetails(objectNumber: String): Single<MuseumDetails> =
        museumsRepository.getDetails(objectNumber = objectNumber)
}