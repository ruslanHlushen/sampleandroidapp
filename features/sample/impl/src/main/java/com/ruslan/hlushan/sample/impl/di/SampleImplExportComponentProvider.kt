package com.ruslan.hlushan.sample.impl.di

import com.ruslan.hlushan.core.api.di.AppContextProvider
import com.ruslan.hlushan.core.api.di.LoggersProvider
import com.ruslan.hlushan.core.api.di.SchedulersProvider
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import com.ruslan.hlushan.core.api.utils.NetworkConfig
import com.ruslan.hlushan.parsing.impl.di.ConverterFactoryProvider
import com.ruslan.hlushan.sample.core.api.di.MuseumsInteractorProvider
import com.ruslan.hlushan.sample.core.api.dto.KeysConfig
import com.ruslan.hlushan.sample.impl.BuildConfig
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [SampleImplModule::class],
    dependencies = [
        LoggersProvider::class,
        SchedulersProvider::class,
        AppContextProvider::class,
        ConverterFactoryProvider::class
    ]
)
interface SampleImplExportComponentProvider : MuseumsInteractorProvider {

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance initAppConfig: InitAppConfig,
            @BindsInstance networkConfig: NetworkConfig,
            @BindsInstance keysConfig: KeysConfig,
            loggersProvider: LoggersProvider,
            schedulersProvider: SchedulersProvider,
            appContextProvider: AppContextProvider,
            converterFactoryProvider: ConverterFactoryProvider
        ): SampleImplExportComponentProvider
    }

    object Initializer {
        fun init(
            initAppConfig: InitAppConfig,
            networkConfig: NetworkConfig,
            loggersProvider: LoggersProvider,
            schedulersProvider: SchedulersProvider,
            appContextProvider: AppContextProvider,
            converterFactoryProvider: ConverterFactoryProvider
        ): SampleImplExportComponentProvider =
            DaggerSampleImplExportComponentProvider.factory()
                .create(
                    initAppConfig = initAppConfig,
                    networkConfig = networkConfig,
                    keysConfig = KeysConfig(
                        apiUrl = BuildConfig.API_URL,
                        apiKey = BuildConfig.API_KEY
                    ),
                    loggersProvider = loggersProvider,
                    schedulersProvider = schedulersProvider,
                    appContextProvider = appContextProvider,
                    converterFactoryProvider = converterFactoryProvider
                )
    }
}