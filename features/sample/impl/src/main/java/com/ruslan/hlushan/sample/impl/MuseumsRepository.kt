package com.ruslan.hlushan.sample.impl

import androidx.annotation.IntRange
import com.ruslan.hlushan.core.api.dto.PaginationResponse
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.managers.SchedulersManager
import com.ruslan.hlushan.sample.core.api.dto.KeysConfig
import com.ruslan.hlushan.sample.core.api.dto.MuseumBaseInfo
import com.ruslan.hlushan.sample.core.api.dto.MuseumDetails
import com.ruslan.hlushan.sample.impl.remote.MuseumsRemoteApi
import io.reactivex.Single
import javax.inject.Inject

internal class MuseumsRepository @Inject constructor(
    private val keysConfig: KeysConfig,
    private val museumsRemoteApi: MuseumsRemoteApi,
    private val schedulersManager: SchedulersManager,
    private val appLogger: AppLogger
) {

    fun loadPage(
        nextId: Int?,
        @IntRange(from = 1) pageSize: Int,
        search: String
    ): Single<PaginationResponse<MuseumBaseInfo, Int>> =
        museumsRemoteApi.loadPage(
            apiKey = keysConfig.apiKey,
            pageNumber = (nextId ?: MuseumsRemoteApi.FIRST_PAGE_NUMBER),
            pageSize = pageSize,
            search = search
        )
            .map<PaginationResponse<MuseumBaseInfo, Int>> { response ->
                val mappedResult = response.artObjects.map { dto -> dto.toEntity() }

                if (response.artObjects.size < pageSize) {
                    PaginationResponse.LastPage(result = mappedResult)
                } else {
                    PaginationResponse.MiddlePage(
                        result = mappedResult,
                        nextId = (nextId ?: MuseumsRemoteApi.FIRST_PAGE_NUMBER).inc()
                    )
                }
            }
            .doOnError { error ->
                appLogger.log(this@MuseumsRepository, "loadPage: ERROR", error)
            }
            .subscribeOn(schedulersManager.io)

    fun getDetails(
        objectNumber: String
    ): Single<MuseumDetails> =
        museumsRemoteApi.getDetails(apiKey = keysConfig.apiKey, objectNumber = objectNumber)
            .map { response -> response.artObject.toEntity() }
            .doOnError { error ->
                appLogger.log(this@MuseumsRepository, "getDetails: ERROR", error)
            }
            .subscribeOn(schedulersManager.io)
}