package com.ruslan.hlushan.sample.impl.remote

import android.content.Context
import com.ruslan.hlushan.core.api.log.AppLogger
import com.ruslan.hlushan.core.api.utils.InitAppConfig
import com.ruslan.hlushan.core.api.utils.NetworkConfig
import com.ruslan.hlushan.network.NetworkBuildHelper
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import javax.inject.Inject
import kotlin.reflect.KClass

internal class NetworkApiCreator
@Inject
constructor(
    private val initAppConfig: dagger.Lazy<InitAppConfig>,
    private val networkConfig: dagger.Lazy<NetworkConfig>,
    private val appLogger: dagger.Lazy<AppLogger>,
    private val context: dagger.Lazy<Context>,
    private val converterFactory: dagger.Lazy<Converter.Factory>
) {

    fun <T : Any> createApi(service: KClass<T>, cacheFolderName: String, baseUrl: String): T {
        val nonAuthorizedOkHttpClient = NetworkBuildHelper.provideOkHttpClientBuilder(
            initAppConfig = initAppConfig.get(),
            networkConfig = networkConfig.get(),
            baseUrl = baseUrl,
            appLogger = appLogger.get(),
            context = context.get(),
            cacheFolderName = cacheFolderName
        )
            .build()

        val apiRetrofit =
            provideApiRetrofit(nonAuthorizedOkHttpClient, baseUrl, converterFactory.get())
        return apiRetrofit.create(service.java)
    }

    private fun provideApiRetrofit(
        okHttpClient: OkHttpClient,
        baseUrl: String,
        converterFactory: Converter.Factory
    ): Retrofit =
        NetworkBuildHelper.provideRetrofitBuilder(converterFactory, okHttpClient)
            .baseUrl(baseUrl)
            .build()
}