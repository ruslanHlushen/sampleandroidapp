package com.ruslan.hlushan.sample.impl.remote.dto

import com.ruslan.hlushan.sample.core.api.dto.Image

internal class ImageDTO(
    val url: String?
) {
    fun toEntity(): Image? =
        if (this.url != null) {
            Image(
                url = this.url
            )
        } else {
            null
        }
}