package com.ruslan.hlushan.sample.impl.di

import com.ruslan.hlushan.sample.core.api.MuseumsInteractor
import com.ruslan.hlushan.sample.impl.MuseumsInteractorImpl
import dagger.Binds
import dagger.Module
import dagger.Reusable

@Module(includes = [SampleRemoteHttpApiModule::class])
internal interface SampleImplModule {

    @Binds
    @Reusable
    fun provideMuseumsInteractor(impl: MuseumsInteractorImpl): MuseumsInteractor
}