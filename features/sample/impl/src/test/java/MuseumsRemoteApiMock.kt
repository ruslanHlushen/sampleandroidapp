import com.ruslan.hlushan.core.api.dto.OperationResult
import com.ruslan.hlushan.core.api.dto.getOrThrow
import com.ruslan.hlushan.sample.impl.remote.MuseumsRemoteApi
import com.ruslan.hlushan.sample.impl.remote.dto.MuseumsDetailsResponseDTO
import com.ruslan.hlushan.sample.impl.remote.dto.MuseumsPaginationResponseDTO
import io.reactivex.Single

internal class MuseumsRemoteApiMock : MuseumsRemoteApi {

    var receivedPageNumber: Int? = null
        private set

    var museumsPaginationResponseDTO: OperationResult<MuseumsPaginationResponseDTO, Throwable>? =
        null

    override fun loadPage(
        apiKey: String,
        pageNumber: Int,
        pageSize: Int,
        search: String
    ): Single<MuseumsPaginationResponseDTO> {
        receivedPageNumber = pageNumber
        return Single.fromCallable { museumsPaginationResponseDTO?.getOrThrow() }
    }

    override fun getDetails(
        objectNumber: String,
        apiKey: String
    ): Single<MuseumsDetailsResponseDTO> {
        TODO("Not yet implemented")
    }

    fun cleanUp() {
        receivedPageNumber = null
        museumsPaginationResponseDTO = null
    }
}