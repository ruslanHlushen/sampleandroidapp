import androidx.annotation.IntRange
import com.ruslan.hlushan.core.api.dto.OperationResult
import com.ruslan.hlushan.core.api.dto.PaginationResponse
import com.ruslan.hlushan.core.api.log.EmptyAppLoggerImpl
import com.ruslan.hlushan.core.api.managers.CurrentThreadSchedulersManager
import com.ruslan.hlushan.sample.core.api.dto.KeysConfig
import com.ruslan.hlushan.sample.impl.MuseumsRepository
import com.ruslan.hlushan.sample.impl.remote.MuseumsRemoteApi
import com.ruslan.hlushan.sample.impl.remote.dto.MuseumBaseInfoDTO
import com.ruslan.hlushan.sample.impl.remote.dto.MuseumsPaginationResponseDTO
import com.ruslan.hlushan.test.utils.generateFakePositiveInt
import com.ruslan.hlushan.test.utils.generateFakeStringId
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test

internal class MuseumsRepositoryTest {

    private lateinit var museumsRemoteApi: MuseumsRemoteApiMock
    private lateinit var museumsRepository: MuseumsRepository

    @Before
    fun before() {
        val keysConfig = KeysConfig(
            apiUrl = generateFakeStringId(),
            apiKey = generateFakeStringId()
        )
        val scheduler = CurrentThreadSchedulersManager()
        val logger = EmptyAppLoggerImpl()

        museumsRemoteApi = MuseumsRemoteApiMock()
        museumsRepository = MuseumsRepository(
            keysConfig = keysConfig,
            museumsRemoteApi = museumsRemoteApi,
            schedulersManager = scheduler,
            appLogger = logger
        )
    }

    @After
    fun after() {
        museumsRemoteApi.cleanUp()
    }

    @Test
    fun assertErrorNotHided() {
        val error = IllegalStateException("error")
        museumsRemoteApi.museumsPaginationResponseDTO = OperationResult.Error(error)

        val testCompleteObserver = museumsRepository.loadPage(
            nextId = null,
            pageSize = 100,
            search = ""
        )
            .test()

        testCompleteObserver
            .assertNotComplete()
            .assertNoValues()
            .assertError(error)
    }

    @Test
    fun assertFirstPagePappedCorrect() =
        assertPage(nextId = null, mappedPageNumber = MuseumsRemoteApi.FIRST_PAGE_NUMBER)

    @Test
    fun assertValidPageNotMapped() = repeat(10) { index ->
        assertPage(nextId = index, mappedPageNumber = index)
    }

    @Test
    fun assertEmptyPageResponse() =
        assertPageResponse(
            artObjectsSize = 0,
            nextId = generateFakePositiveInt(),
            pageSize = 1,
            responseNextId = null
        )

    @Test
    fun assertPageResponseLowerThenPageSizeToOne() {
        val pageSize = 20

        assertPageResponse(
            artObjectsSize = (pageSize - 1),
            nextId = generateFakePositiveInt(),
            pageSize = pageSize,
            responseNextId = null
        )
    }

    @Test
    fun assertPageResponseHalfPageSize() {
        val pageSize = 20

        assertPageResponse(
            artObjectsSize = (pageSize / 2),
            nextId = generateFakePositiveInt(),
            pageSize = pageSize,
            responseNextId = null
        )
    }

    @Test
    fun assertPageResponseEqualPageSize() =
        (2..10).forEach { pageSize ->
            val nextId = generateFakePositiveInt()
            assertPageResponse(
                artObjectsSize = pageSize,
                nextId = nextId,
                pageSize = pageSize,
                responseNextId = nextId.inc()
            )
        }

    @Test
    fun assertPageResponseGraterThenPageSizeToOne() {
        val nextId = generateFakePositiveInt()
        val pageSize = 20

        assertPageResponse(
            artObjectsSize = (pageSize + 1),
            nextId = nextId,
            pageSize = pageSize,
            responseNextId = nextId.inc()
        )
    }

    @Test
    fun assertPageResponseDoublePageSize() {
        val nextId = generateFakePositiveInt()
        val pageSize = 20

        assertPageResponse(
            artObjectsSize = (pageSize * 2),
            nextId = nextId,
            pageSize = pageSize,
            responseNextId = nextId.inc()
        )
    }

    private fun assertPage(nextId: Int?, mappedPageNumber: Int) {
        val testCompleteObserver = museumsRepository.loadPage(
            nextId = nextId,
            pageSize = 100,
            search = ""
        )
            .test()

        assertEquals(museumsRemoteApi.receivedPageNumber, mappedPageNumber)

        testCompleteObserver
            .assertNotComplete()
            .assertNoValues()
    }

    private fun assertPageResponse(
        @IntRange(from = 0) artObjectsSize: Int,
        nextId: Int,
        @IntRange(from = 1) pageSize: Int,
        responseNextId: Int?
    ) {
        val artObjects = (1..artObjectsSize).map { _ -> generateRandomMuseumBaseInfoDTO() }

        museumsRemoteApi.museumsPaginationResponseDTO = OperationResult.Success(
            MuseumsPaginationResponseDTO(artObjects)
        )

        val testCompleteObserver = museumsRepository.loadPage(
            nextId = nextId,
            pageSize = pageSize,
            search = ""
        )
            .test()

        val responseItems = artObjects.map { dto -> dto.toEntity() }

        val response = PaginationResponse.create(result = responseItems, nextId = responseNextId)

        testCompleteObserver
            .assertComplete()
            .assertNoErrors()
            .assertValue(response)
    }
}


private fun generateRandomMuseumBaseInfoDTO(): MuseumBaseInfoDTO =
    MuseumBaseInfoDTO(
        id = generateFakeStringId(),
        objectNumber = generateFakeStringId(),
        title = "title",
        webImage = null,
        headerImage = null
    )