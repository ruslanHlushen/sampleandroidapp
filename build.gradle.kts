buildscript {

    repositories {
        google()
        jcenter()
        mavenCentral()
    }

    dependencies {
        classpath(Plugins.androidGradlePluginClasspath)
        classpath(Plugins.kotlinPluginClasspath)
        classpath(Plugins.proguardPluginClasspath)
        classpath(Plugins.scabbardPluginClasspath)
    }
}

plugins {
    id(Plugins.androidJarPlugin).version(Versions.androidJarPluginVersion)
    id(Plugins.detektPlugin).version(Versions.detektVersion)
    id(Plugins.scabbardPlugin).version(Versions.scabbardPluginVersion)
}
val androidJar = androidjar.find(ApplicationConfigs.targetSdkVersion)

allprojects {
    val rootProjectPath = project.rootProject.projectDir.path

    val gradleSupportFolder = "$rootProjectPath/gradle_support/"

    extra["proguard_support_folder"] = "$rootProjectPath/proguard_support/"
    extra["lintersConfigFolder"] = "$rootProjectPath/linters/"

    extra["gradle_support_base_kotlin"] = "${gradleSupportFolder}base_kotlin.gradle"
    extra["gradle_support_base_android"] = "${gradleSupportFolder}base_android.gradle"
    extra["gradle_support_kapt"] = "${gradleSupportFolder}kapt.gradle"
    extra["gradle_support_apk_signing"] = "${gradleSupportFolder}apk-signing.gradle"
    extra["gradle_support_build_variants"] = "${gradleSupportFolder}build_variants.gradle"
    extra["gradle_support_android_linters"] = "${gradleSupportFolder}android_linters.gradle"
    extra["gradle_support_dagger2_kapt"] = "${gradleSupportFolder}dagger2_kapt.gradle"

    repositories {
        google()
        jcenter()
        maven(url = "https://jitpack.io")
        maven(url = "https://maven.google.com")
    }
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

apply(from = "${project.rootProject.projectDir.path}/gradle_support/linters.gradle")